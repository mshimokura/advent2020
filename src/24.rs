/*

  nw ne
 w     e
  sw se

 0 1 2 3 
0 1 2 3
 0 1 2 3 # origin
  1 2 3
*/

#![allow(dead_code)]
use std::collections::HashSet;

fn main() {
	println!("p1: {}", p1(INPUT));
	println!("p2: {}", p2(INPUT, 100));
}

fn p1(input: &str) -> usize {
	let flipped = get_black_tiles(input);
	flipped.len()
}

fn print_tiles(tiles: &HashSet<(i64,i64)>) {
	for y in (tiles.iter().map(|(_,y)| *y).min().unwrap())..=(tiles.iter().map(|(_,y)| *y).max().unwrap()) {
		if y % 2 == 0 { print!(" "); }
		for x in (tiles.iter().map(|(x,_)| *x).min().unwrap())..=(tiles.iter().map(|(x,_)| *x).max().unwrap()) {
			print!("{} ", if
				tiles.contains(&(x,y)) { if x == 0 && y == 0 { "O" } else { "x" } }
				else if x == 0 && y == 0 { "o" }
				else { "." });
		}
		println!("");
	}
	println!("");
}

fn p2(input: &str, steps: usize) -> usize {
	let mut tiles = get_black_tiles(input);

	for _ in 0..steps {

		//print_tiles(&tiles);

		let mut next = tiles.clone();
		for t in &tiles {
			let mut black_neighbours = 0;
			for n in &neighbours(*t) {
				if tiles.contains(&n) {
					black_neighbours += 1;
				}
			}
			if black_neighbours == 0 || black_neighbours > 2 {
				next.remove(t);
			}

			for t in &neighbours(*t) {
				if tiles.contains(t) {
					continue;
				}

				let mut black_neighbours = 0;
				for n in &neighbours(*t) {
					if tiles.contains(&n) {
						black_neighbours += 1;
					}
				}

				if black_neighbours == 2 {
					next.insert(*t);
				}
			}
		}

		tiles = next;
		//println!("{}", tiles.len());
	}
	//print_tiles(&tiles);

	tiles.len()
}

fn get_black_tiles(input: &str) -> HashSet<(i64,i64)> {
	let mut coords = Vec::new();
	for line in input.split("\n").filter(|l| l.len() > 0) {
		let mut x = 0;
		let mut y = 0;
		let mut i = line.chars();
		loop {
			match i.next() {
				None => break,
				Some('e') => x += 1,
				Some('w') => x -= 1,
				Some(ns) => {
					match ns {
						'n' => y -= 1,
						's' => y += 1,
						_ => panic!("bad ns"),
					};
					match i.next() {
						Some('w') => if y % 2 == 0 { x -= 1; },
						Some('e') => if y % 2 != 0 { x += 1; },
						_ => panic!("bad n/s ew"),
					};
				},
			}
		}
		coords.push((x,y));
	}

	let mut flipped = HashSet::new();
	for c in coords {
		if flipped.contains(&c) {
			flipped.remove(&c);
		} else {
			flipped.insert(c);
		}
	}

	flipped
}

fn neighbours((x,y): (i64,i64)) -> [(i64,i64); 6] {
	if y % 2 == 0 {
		[
			(x+1,y),
			(x-1,y),
			(x,y+1),
			(x+1,y+1),
			(x,y-1),
			(x+1,y-1),
		]
	} else {
		[
			(x+1,y),
			(x-1,y),
			(x,y+1),
			(x-1,y+1),
			(x,y-1),
			(x-1,y-1),
		]
	}
}


#[test]
fn test_p1_example() {
	assert_eq!(10, p1(EXAMPLE));
}

#[test]
fn test_p2_example() {
	assert_eq!(15, p2(EXAMPLE, 1));
	assert_eq!(12, p2(EXAMPLE, 2));
	assert_eq!(2208, p2(EXAMPLE, 100));
}

#[test]
fn test_p2_simple() {
	assert_eq!(1, p1("nw"));
	assert_eq!(2, p2("nw\nsw", 1));
	assert_eq!(4, p2("nw\nsw", 2));
	assert_eq!(6, p2("nw\nsw", 3));
/*

. . . .
 . x . .
. . O .
 . x . .

. . . .
 . . . .
. x x .
 . . . .

. . . .
 . x . .
. x x .
 . x . .

. . . .
 x x x .
. . . .
 x x x .
*/
}

const EXAMPLE : &str = "
sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew
";

const INPUT : &str = "
seseswnwseneweseswseswnwnese
neneeneseneeeesenewwne
swswswneswsewswseswesewswseswneeswswnwsw
eneeswnwneneenwneneneeneneswnwneswne
seeseseeeeseenenenwswseweese
seswweseseseswseswneswwnwswswseneswnw
wswseswswneeswswsw
wnewnwnewwwwwwwwwsewwsw
wseswnenwneswwnwseeenwnwnwewesewse
seweseseneeeseeswsenwseeenwseeeew
seswswnwswswswnesesesewseeneseswsesesesw
wsenewsenwwnewswwewnwnwsewwwsw
swwwnewnwwseswswnwswenwswseeswesw
senenwswwnwnewwseseseswswwweneswwne
nwseseeeesweee
swwswswswwswswsweswnwwswsw
sewnwswneneneenenewneeneeneneswwee
swenenwswnwnenwnwnwenwnenesenwnenwnwnwwnw
eweeneswnesenesewneneeeneseneneew
enesesenwsweeeeeenwnwnwsweswew
nweeeswneswenwseswnweeeneenenesw
neeeswnwnenwnewnwnwswenwwwnwsesenw
neswnwenwneswnwnwnwneswnesenwnw
neeseeweneeswweeswneneneeneenenw
nenwnenwnenwewnewsenwnwnwnwnene
nwewnwnwwnenwnwwnwnwwnwwenwnwsenw
seseswsewneswseseseseseswwseseswe
eseswnewenwwnenene
wswneneswswsewnwseswswswseswswwwswswne
neeswneneewnenenenenwnewneeeneneswse
eneeneneneneeeneneenewsene
eneweewwsenesene
nwenwenwnwswnenwsenwnesenenenenwnesw
wesenesenewweewwwswwnwsew
eeswneeewsewneneneweneeneene
eeswswswnwweswnenwsw
swesewnwneswseesesesesesenwsenwesee
enwsenwnwswnwwnwseseseseeswewnesenw
eeseeeseenwseenweeeenwesese
nenwnwswneneswneenenwneneswnenwnwnwnenee
ewswwswwswewwnwwenww
swnenenwnenenenesenenesewnenwnenwesenesw
nwseeenwnwwnwnwenwswnwswnwnwneenwnew
ewwnwwwwnwswnwnenwsenwnwewnwww
eswwwseewsewweeswwnwnwwenwwwne
wswseswswswnenwswwswswnewswswse
nwnenwseseswsesesenwseseseweswew
swesweenenwnwnenwnenwwneneseenenwsw
wswwwswneswwwswwewnenwswwwew
wsewwwnwwwwnwsenenewnwewwsw
wswswwesweswnwswwwswswsweswswswsw
eswswwnwwswsesweswswswneswswnwnwsesw
neenwsewwswneseswwseswnwwwwswww
enwseweseseseeswseneneseswseswneee
swnwswswneseswswswsesewseseneneseswsee
wsenwwswwwnwswwwwneswewwneww
nweswsweeewnenesee
swsweswnwswseneswswswseswswswsw
swsewseswseeseneeswswwswswnenenwnesesw
seeeenenewenenenenesew
nwnesewsesesenwsesenwsenwnwnenwnenwnww
enwswsewnesesesenwe
nwnwnesenwwwwnwnw
nenesenewneenenenenwswneenesenenwnewne
wnwnesenenwesewsenenenenwneeeneswse
wwswswswswswswwswwswesww
enwsesesewseeesweneeeswesesweenw
wwnwseneneneeenewswnwnwneswsenewnwsenw
swswswswswswewswswwswsww
nenwwnenwsenwnwswseswnwneneseseesenww
seeseeseeeesenewseswweseseswnenese
wwsewwwewneeswnwwwewwwenesw
wenewseeeneneenenenenenwswswnenwnee
neswnwnwesesenwswewneewsenwesweswswse
nesweewswsewswnesewnewswwwwwsw
nwneeeneseneneeneswenenwswnewsenwe
swswswswnwswwswswesweeesenwswwswse
senenwewnenenenenwnwneneswneenenenewne
senwseneesweeesenenwseseseneewwe
ewwwwwnesenesenwnwsewsesenenewswnw
sewswswswseneswseseswseswneswswse
eenwneneeeeeewswneseneeew
swwwwseswswnweswwnwenenweenwseeene
eeneeeeswnwneseenwesweeenweee
enwwwwnwnwwwnwnwneseswswnwnwnwwenwnw
wwwswswswswwswewswsw
swwwwnesewswewwswneswseenw
seeneeeeeweeeseee
swneswwnwsenweswnweswswnwseeseseseswnw
swswseswnwswswsweseswnwswswswneswsesew
neneswswwwwwswwnwswswwswsesweswsesw
sweneswnesenenwnenwesenewnwnewnwwene
nwnwnenenwwnesweeswnwswseseneseewnw
nenwnenenwneneneswneneenenwswnenw
nwseswseswwwswnwneswswwswwwwswnese
esenwenwnwwnewnwnwneswnwnwwnwnwnwnw
wwwswsenwwswwwwwwnwnwwsewwe
wesenweneeeseswseeneswswnenwwesw
swswnwseseswsenwnenwnwseewweswswseene
swswnewswswneswswswsenwweswwswswswsww
eswwnenwnwneewenweswswnewweswese
seneenwsewwnwnee
swswswswsenwsweswswswenwswsenwswsesesw
nwnweenwnwnenwnwneswswnwnwsenwnwnwnwnwne
wnwwnwwwwwwsew
nwswnwnwnenenwnwnwswnwseneswnwnwnwne
newwneeeswseneeneneneneweeeenene
eeenwnwneeeeeeesweenwswwseew
esenewwwwnwswswswswwwnwswswswww
eeweeeseseeesee
wewwnenewswwneseswwnwwwseswswww
wweenenewswswnwwewwswwwswswsww
seseeswneesewnwsenwsesesesenwwswnwsee
nwnwnenwnenwnwnwsesenwnwnwwnwnenenwnwse
eswswseseswwswseseswswnweseswenwsesw
eeseneneeesenenwswwweewneenene
swsenwwswnwnwnwswnwenweneswneeneswnw
nenwenenewseneweewswneenesweene
swwwwwwwnwswew
seesenwesesesewnwsenweseseswseswsee
nenenwnwnenewnwnwnenwseswnwnwnwnenwseswse
neneeeenwwweseseswewseesenwswnw
enewesenenesenwwsesweneeneeneenenee
seeeeeneewseewseseeeeesesewnw
wwswswswswwwnwwewswsw
wswnwnenweswnwnwnwnwnwnwsenwewse
nwnwnwnwswnenenwenwenwnwneseswnwnwnenwne
ewswswswwnwswswwswnwswswswswwwsesenwne
wnwsenesenwsenwesewewseswnwnenwneswne
nesweenenwneeneseeeneneseenewneenw
nwswswswesenwsenwesweseswsewswesenwsw
sweswneneeneswnwesenwwnwseeseswwnw
enwnwnwnwnenwnwnwnenwwnwesewnwnwsenwnw
eneseweeeneneneswwseneswweswswsw
swswwwseneeseeewnwwseswnwswenwneswnw
senewwswsesenewnwseseswswswneseneswsesw
swnwnwnenwsenwsenwnwsenwnwwnwnwew
seseseseswneesenweseeseswsesenwsewsese
eewweeneneswswenenweneneeeesw
ewneneeeseeeneneesweswenweswene
wseeneseeswnwwesesesenwnwwsesee
sesesesewseseneseseswnesesesenwsesenwse
seseswseneneewseseseseseseesesee
swswsesenwneeseswsesesesesesesew
nenwseswswnesenewwnwenwseeeeneneenene
neswneswswnewnwswnwseneeneneenwenwne
swnwnwnwnwnwseenewnenenwnwnwnweewnwnw
swnwnewnwwnwnwwnewswwsewwnwswnewew
nwnenwswnenwnwnwnenesenwsenenwnenenesenwnw
nenwewseswneweesweseeenweswswnw
swsweswseeswwswswswneswwwnwnwswnwse
swwwswwsewenewsewswswwnewnwswnw
swenenwnweswnwswwswwwnwweswnewnw
nwnenwnwnwnwwenenwnwsenenewenwnwswnesw
neesewswnwswseswnwenenwwswswswswewse
seseseseeeseseseswnw
swswenwswnenwnwnenwnwnwsenenwnwnenenwnwnew
wseseeeseseneseswesesesesese
seswseseseseswneswnwwseseseswesw
nwnwnwnwenweswsesenwswnwnwnwnwnwewswnwne
swneneenenwnenenewneneeweeeeswnese
wwnwnewwwwwswwww
eseeeseeeeenwewseenwesenwwse
seswseswneswseswswswwseswsw
wneswseswswnwswneseswsweswneswwswswsesw
wswwnwnwnwnwnesweswseswseswenwwswew
neeeeswneeswnweeeeeeesweee
neswswnwswesenwsenesenwsewseewswswne
newseesesesenwsenwewsewsese
sewseenwseseseswseseeswseseseneenwse
weseneeeeeesewneseneeswewsew
wswnwnwsewnwnwenwewwnwsesenwnenwnwnw
senenenenenewnenwnwse
seseseseseswsesenesesesesewsenwseneese
swwneewwneswsewseseeneenwsenenese
swnwnwneswseswsweswewneswnewnweseswsw
swswswswseeswswnwseswswnw
nenwseneenwneneenwneneneswseseeneswenw
swneswswwswswseswswswswnweswewswswe
swnenenwneswenenewnwnwnenenenese
nwswswnewseswnwswnwwwesweeswesese
nwenwnwnwnenwnwnesenwnwnwswnwnwsesenese
sweewwnwswesewnenwswww
eweneeswwswneeneeeseesenenwnee
nwnwnwnwsewnwenwnwnweswwwnenwnwee
seswswswwswnwwseneswswswswneswwswnene
newnenenwnesenenesewneswnenwnwnwnenwne
sewnenenenwneeswwenwneswnese
swnwnwwnwnwnenwenenesenewneeswnenene
swnenenesenewneneswsewwwwneesesee
wewneneeneneswwwsenenesene
wenwswswswnwwenwwwwenewnwnwwww
wwewseswwwswswwwnewnewnwnwwwne
swswwswswnwswnwswseswswswnwswswswseeswne
wewswnweneeseseeweeeewwsene
swsewwsenwseswewneswsesee
seseswnwneneseesesesesesesenweewsesese
seseseseswnesesesenwswsesew
swnwseswewseswswswswswse
wwesenwnwnweswswnwneewswnwneewse
nwnwnwnwnwnesesenwnwsenewnwnwnwnwnwnwnw
wswwwnenwswseswewwswswwnwwswswsw
senenenewnenesenenenenenwenweese
swneneeneswenewswnenwnweneesee
neenenewneeewneneswseneseneneneene
swnewswswsenwwswseswnweswswnwsweswsw
ewwnwenwwwsewnwnwnwsesw
seswseswswswseswsesenwsese
neneswnweeeeeneeswwnwswseee
swnenwnenwnwneswenenenwnenenweneneswnee
nenenwnwneneneneswnenenwnwsenwnewesenwwe
wwnwnwnenwwwwewseeswwwnwnwswnwnw
neneeneeeeneneeeeewe
nwneswweswswnesweesenwswswswswsesewse
seewseneseneseseseswswewseeseenwsee
nwswseswswseseswseswseswse
swseewnwseswwwseeswswnesewneneese
nwneeseeeneeeeeeee
wwswenwwnwnwwwsenwwwwwnwsenew
weneneneeneneswne
nenwswneswswwswwwwwswenewwsesewsw
sesesenenwneswwenesenewesweswseeenw
sweewswwnwenwseseseenwsesw
enwneenwewsenwnwwsewwwsww
nwnweseneswneeneneneweswswwneseswnese
nweseswneeseenwnenenesesenwenenenwwe
nwwnwnwsenwnwnwenenwnwnwsenwswnwsenwwnw
wewwwwseswnwsewwwnenwwwwsenesw
seswseswswswswseseswnwswswesw
sweswwnewneneneneneseneneneenwenene
swwnwswswwwewswswenwwse
senwenwnwwnwesesenwwnwnwnenwsenwnwnwne
seseeeweseswwnenwsenesewnweseeese
nwwewwnwnenwswnwsewswwnewenwnwnwnw
eesweesenweewneeeeenwsweene
newswesenwnewswneswswswseswswnwswnesw
sesesewwsesewseseneeswsenesesenesese
nwnwwswnwnenwwnwwnwnwneseswesenwswnwse
wwnesewnwnesesewsewnwwneneswse
eeweneeneweneeseeewneeeene
neneswnenwnenenwnenenwnwnwene
wwwseswswneenewseswseswwswnenesese
nwnwnwnwseswwnwswnwnwsenwnwsenwsenenwnee
newswswwwwseseneswwswswnewswwww
seseseseneseseswswswseseesenesesewswnwse
eeseswweseeenwsenesesesesesewsesee
neenenenenenwnweesenwesenwenesenesee
nwnwnwnenwswnwnwsenwnwnwnenewnwnw
seswnewswswswswnwe
eswnewneeneneneenenenwnenewnwewswse
wnewnenenwnwseneneseswsewenwsesenwwwse
eeseseweneseseewnesewnesesesenw
seeneeeewseeeneewsesweeseee
seseseswswsenwneeswnwswseseseswseswswnese
sewwswswseneswnwwwwswwne
nwnwnwnenwnwnwwnwnweswnwnwswenw
seswnwseseswnwnwseseenenw
newsewswnwwwneswsewneeeseswnwwenenw
neneeswnenenewseesenewe
seswneswsesewseeswse
sesenwnewneneeeneneneenenenesewnenenenw
wwneneswewswswsew
sewwwnwnwnwwsenwnenwsesenww
nwwnwnenenwwnwwswswswewseneesenewwnw
eeswswneneneneeneeeeseeewneee
wsewswwwnenwswnwswwsenenwenenw
nwwnwwwwnwewnewwwsenwswwswenwne
swsenesenwsesesewseswneeseseseseswsenw
wswswswseeswswswswnwswneweseswswswswnw
wnwnwwnwnwwnwnwwnwwswnwwenwwese
swseesenwswnwsenwswsweeweenwneeenene
neswnenewneneneneneneneneswnesenenesew
wnwneenenewnenenesenwne
senwnwseenesewseswwseswsweswsenenwsee
nwnwnwwnwenwsweswwnwseenwwnwnwnwnw
neseneneneswnwswsenewnewneenwsewnenenese
nwseseswwswseesenesenese
swsenwsewwneesewweswnenwneenwwnwse
eswswwswnenewseseswnwseswswnewswswsesw
wewneseeswwwnwneewnwswwswswwsesw
swseeewseswwswswneswswnwswsw
seewswwswwswsenwnwswwswnewswswswsww
seswswswsesesenewswswsenwswswswneswswsw
enwwwwswswwwswsewswnwnwseeswnwwsw
seneseneswseswseseswwwsweseswnwesesese
eswnesweswswswswnwswswswswwwsw
eeeeweeseseneewe
swnenwenwnwnenesewwswnwnwnwnwne
seswneswswwwsewenewwswwswwwww
seswseseswswswswswsenwsesenwneeewseswswse
wwnwnwwwenwwnwnww
seswseeseeseeneesesesewnese
wnwnwwnesewwwswewsesewnwewwe
swnwseswswswnweswneweswswswswswswnesw
swseseseseseswswseswne
nwswwwwnwswnwsesewenewwsesenwnenw
swwnwsweeewnwnwwsweseswsw
nwnwnwnweswnwsenwnwnwnwnwnwnwnww
eswnenwswnwneeneee
enweneeeeeeesenenee
nwwnwweneeseenenenwnwnwwswswnenenw
eeeneseesweeeewenwesweeee
swswnewswwsesenwwswswwwwewneswswnesw
seseseseseseseesesewesesewe
neswwwwwneswwswwwwsewneneww
wsewwwwwwnwnwnwwnwnww
nenwneneneneneseneneenenenewneswnwesene
swnwseseswswswseweswsweswswnwnesewseesw
weswswneswseewwnwnwneseesese
eewwnwnweswww
swwwwwwwswsewwewswnewwneww
senesesesenwnwnwnenewsenwnwnewnw
nwnenwnwnenenwewseenwnwnenwnwwnewnw
wwwweswwseneneseenwseswnwwnwwwnw
nwnwwswnenwnwnwwwseewswnwnenweww
neswnwnewneneeneneneneneneneneseswnesene
eeenwewseeenwsewnwsewe
nwnwwnwwnesenwnwnwnwnwnwsenwnwswnenwnw
nwseswswswnewnenwnwneneenenwwneneeesw
nwnwnwnwwswsenweenwnweenwnwnwswnwnww
nwesweeeeenweeseweeeeewee
swseseswseswswswswnwnwswswsweneswswsese
swesesweeswswswnwwswswswsewse
wswswweseswswswenwswnwswswnwwwwsw
wsesesesenwswswnwseseseeseseesesesese
wsenewwwwwwwwnewsew
ewswwwwswwwwwnwweswww
enwwnwnenwesewne
wswswseswsesesenesesesese
swseseseswseseseswswnewe
swswnwnenenwswneneneneenenenenenwswnenw
eneeseeseenewseeseewe
nwneseswswswswswnwswswswswwneeseswswne
swswsesesesenwseseesenew
newnwsenwsenenwnwnenwnwnwnesenwwnwnw
eneenenewwneneneneneneseneenesene
wnwseweeenwswsewwneswwneenewewe
nwnwnwwwnwwneenwsenenwseswnwswwnwnw
nwnwswneneswnwnwnenwwnweswswwnwnenwnw
neneenweseswseeewneweneenweseee
eeneweseeeeenwsenweneeeeswswe
ewsewswnwwwwnwneewseswneenewswwe
eeweeesweeeneeeeswswseenwnwee
nwnweswswsewwsenesweswwnwnwenwswe
wwewwsesenwswnwwsenwnenwwnwsewwnw
eewsewwnewwwwneswwwswswswww
enwewswesenweenwneewseswseeee
sesenewswwwweneneseeswnwnenwneee
nwewnwsewnwnenwwnewwse
eseeseseeesesesenwseneswneneswesew
wswwseenewswenwwweswswnenww
neseswseseeswswswwswswnwsenwsweseswswse
eeswwenwnenwneeweseweswneswene
nenwnenwnwnwneneswnenenenwneenenwswseswnw
swneeeeseenweneseeweswneesenwenwsw
nwnwnwsenwwnwsenwnwne
newwnwsenwnewsewswnenwswwenenwnesese
senwsenwnesenwnewnenwne
";

