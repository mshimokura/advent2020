#![allow(dead_code)]

fn main() {
	println!("p1: {}", p1(&parse(INPUT)));
	println!("p2: {}", p2(&parse(INPUT)));
}

fn p1(input: &[usize]) -> usize {
	let num_cups = input.len();
	let cw = crab(input, num_cups, 100);
	println!("{:?}", cw);

	let mut n = 0;
	let mut r = 0;
	for _ in 0..num_cups-1 {
		r = r*10 + (cw[n]+1);
		n = cw[n];
	}

	r
}

fn p2(input: &[usize]) -> usize {
	let cw = crab(input, 1_000_000, 10_000_000);
	(cw[0]+1) * (cw[cw[0]]+1)
}

fn crab(input: &[usize], num_cups: usize, swaps: usize) -> Vec<usize> {
	let mut cw = Vec::new();

	cw.resize(num_cups, 0);
	for i in 0..(input.len()-1) {
		cw[input[i]-1] = input[i+1]-1;
	}
	if num_cups <= input.len() {
		cw[input[input.len()-1]-1] = input[0]-1;
	} else {
		cw[input[input.len()-1]-1] = input.len();
		for i in input.len()..(num_cups-1) {
			cw[i] = i+1;
		}
		cw[num_cups-1] = input[0]-1;
	}

	let mut current = input[0]-1;
	let mut picked_up = [0,0,0];
	for _ in 0..swaps {

		/*
		let mut n = 0;
		for _ in 0..input.len() {
			print!("{} ", n+1);
			n = cw[n];
		}
		println!("");
		*/

		picked_up[0] = cw[current];
		picked_up[1] = cw[picked_up[0]];
		picked_up[2] = cw[picked_up[1]];
		cw[current] = cw[picked_up[2]];

		let mut next = if current == 0 { num_cups-1 } else { current-1 };
		current = cw[current];

		loop {
			if picked_up.iter().all(|p| *p != next) {
				break;
			}
			next = if next == 0 { num_cups-1 } else { next-1 };
		}

		cw[picked_up[2]] = cw[next];
		cw[next] = picked_up[0];
	}

	cw
}

fn parse(input: &str) -> Vec<usize> {
	input.chars()
		.map(|c| c.to_string().parse().unwrap()) // allocations, bleh
		.collect()
}

#[test]
fn test_p1_example() {
	let input = parse(EXAMPLE);
	println!("{:?}", &input);
	assert_eq!(67384529, p1(&input));
}

#[test]
fn test_p2_example() {
	let input = parse(EXAMPLE);
	println!("{:?}", &input);
	assert_eq!(149245887792, p2(&input));
}

#[test]
fn test_p2_start() {
	let input = vec![3,2,1];
	let cw = crab(&input, 5, 0);
	assert_eq!(3, cw[0]);
	assert_eq!(2, cw[4]);
}

const EXAMPLE: &str = "389125467";
const INPUT: &str = "253149867";

