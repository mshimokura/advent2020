#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;

fn main() -> Result<()> {
	println!("p1: {}", p1(&parse(INPUT)?));
	println!("p2: {}", p2(&parse(INPUT)?));
	Ok(())
}

fn p1(input: &Vec<i64>) -> i64 {
	let counts = std::iter::once(&0).chain(input.iter())
			.tuple_windows()
			.fold((0,1), |(ones,threes), (a,b)| match b-a {
				1 => (ones+1,threes),
				3 => (ones,threes+1),
				_ => (ones,threes),
			});
	counts.0 * counts.1
}

fn p2(input: &Vec<i64>) -> u64 {
	let to_three: &[usize] = &[0,1,2];
	input.iter()
			.fold([(-2,0),(-1,0),(0,1)], |prev,j| [prev[1], prev[2], (*j,
					to_three.iter().filter(|i| prev[**i].0 + 3 >= *j).map(|i| prev[*i].1).sum())])
			[2].1
}

fn parse(input: &str) -> Result<Vec<i64>> {
	let r = Regex::new(r"\d+")?;
	let mut v = r.find_iter(input)
			.map(|x| x.as_str())
			.map(|x| x.parse().map_err(Report::from))
			.collect::<Result<Vec<_>>>()?;
	v.sort();
	Ok(v)
}

#[test]
fn test_example1() -> Result<()> {
	assert_eq!(p1(&parse(EXAMPLE)?), 35);
	Ok(())
}

#[test]
fn test_example2() -> Result<()> {
	assert_eq!(p2(&parse(EXAMPLE)?), 8);
	Ok(())
}

const EXAMPLE : &str = "
16
10
15
5
1
11
7
19
6
12
4
";

const INPUT: &str = "
49
89
70
56
34
14
102
148
143
71
15
107
127
165
135
26
119
46
53
69
134
1
40
81
140
160
33
117
82
55
25
11
128
159
61
105
112
99
93
151
20
108
168
2
109
75
139
170
65
114
21
92
106
162
124
158
38
136
95
161
146
129
154
121
86
118
88
50
48
62
155
28
120
78
60
147
87
27
7
54
39
113
5
74
169
6
43
8
29
18
68
32
19
133
22
94
47
132
59
83
12
13
96
35
";

