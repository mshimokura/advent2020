#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;

fn main() -> Result<()> {
	println!("p1: {}", p1(parse1(INPUT)?));
	println!("p2: {}", p2(parse2(INPUT)?));
	Ok(())
}

fn p1((start, ids): (i32, Vec<i32>)) -> i32 {
	let first_bus = ids.iter()
		.map(|id| (id, id * (start/id+1)))
		.sorted_by(|a,b| Ord::cmp(&a.1, &b.1))
		.nth(0)
		.unwrap();

	first_bus.0 * (first_bus.1 - start)
}

/*
	
	n1*m1 + n2*m2 = 1
	
*/

fn modulo(x:i64,m:i64) -> i64 {
	(x%m + m) % m
}

fn p2(input: Vec<Schedule>) -> i64 {

	let mut bus1 = 0;
	let mut x = 1;

	let mut n1 = 0;
	let mut a = 1;
	for i in input {
		match i {
			Blank => a += 1,
			Bus(n) => {
				if bus1 == 0 {
					bus1 = n;
				} else {
					let n2 = n;

					let (m1,_m2) = bezout(n1,n2);
					x *= m1.abs() * a;
				}

				n1 = n;
				a = 1;
			}
		}
	}

	x * bus1
}

#[test]
fn test_p2() {

/*
	x = 0 mod 3
	x = 2 mod 7
	x isn't the same?

	-1 mod 3
	0 mod 7
*/


	assert_eq!(p2(vec![Bus(3), Bus(7)]), 3*2);
	assert_eq!(p2(vec![Bus(3), Blank, Bus(7)]), 3*4);
	assert_eq!(p2(vec![Bus(3), Blank, Bus(11)]), 3*3);
	assert_eq!(p2(vec![Bus(5), Bus(11)]), 5*2);
	assert_eq!(p2(vec![Bus(5), Blank, Blank, Bus(13)]), 5*2);
}

fn bezout(a: i64, b: i64) -> (i64,i64) {
	let (mut old_r, mut r) = (a, b);
    let (mut old_s, mut s) = (1, 0);
    let (mut old_t, mut t) = (0, 1);
    
    while r != 0 {
        let quotient = old_r / r;
        (old_r, r) = (r, old_r - quotient * r);
        (old_s, s) = (s, old_s - quotient * s);
        (old_t, t) = (t, old_t - quotient * t);
	}

	(old_s, old_t)
}

#[test]
fn test_bezout() {
	assert_eq!(bezout(240,46), (-9, 47));
	assert_eq!(bezout(3*2,7*2), (-2,1));
}

fn parse1(input: &str) -> Result<(i32, Vec<i32>)> {
	let mut start = None;
	let mut ids = Vec::new();

	for s in Regex::new(r"\d+")?.find_iter(input).map(|m| m.as_str()) {
		if start.is_none() {
			start = Some(s.parse()?);
		} else {
			ids.push(s.parse()?);
		}
	}

	Ok((start.ok_or(Report::msg("no start found?"))?, ids))
}

#[derive(PartialEq, Debug)]
enum Schedule {
	Blank,
	Bus(i64),
}
use Schedule::*;

fn parse2(input: &str) -> Result<Vec<Schedule>> {
	Regex::new(r"(\d+|x)")?.find_iter(input).map(|m| m.as_str())
			.skip(1) // skip start time
			.map(|s| match s {
				"x" => Ok(Blank),
				_ => Ok(Bus(s.parse()?)),
			})
			.collect()
}


#[test]
fn test_example1() -> Result<()> {
	assert_eq!(p1(parse1(EXAMPLE)?), 295);
	Ok(())
}

#[test]
fn test_parse2() -> Result<()> {
	assert_eq!(parse2(EXAMPLE)?, vec![Bus(7), Bus(13), Blank, Blank, Bus(59), Blank, Bus(31), Bus(19)]);
	Ok(())
}

#[test]
fn test_example2() -> Result<()> {
	assert_eq!(p2(parse2(EXAMPLE)?), 1068781);
	Ok(())
}

#[test]
fn test_examples2() -> Result<()> {
	assert_eq!(parse2("0 17,x,13,19")?, vec![Bus(17), Blank, Bus(13), Bus(19)]);
	assert_eq!(p2(parse2("0 17,x,13,19")?), 3417);
	assert_eq!(p2(parse2("0 67,7,x,59,61")?), 1261476);
	assert_eq!(p2(parse2("0 1789,37,47,1889")?), 1202161486);
	Ok(())
}

const EXAMPLE : &str = "
939
7,13,x,x,59,x,31,19
";

const INPUT: &str = "
1001171
17,x,x,x,x,x,x,41,x,x,x,37,x,x,x,x,x,367,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19,x,x,x,23,x,x,x,x,x,29,x,613,x,x,x,x,x,x,x,x,x,x,x,x,13
";

