#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::RangeInclusive;
use std::iter;
use std::cmp;

fn main() -> Result<()> {
	println!("p1: {}", p1(parse(INPUT)?));
	println!("p2: {}", p2(parse(INPUT)?));
	Ok(())
}

fn p1(mut state: HashSet<(i32,i32,i32)>) -> usize {

	let mut min = state.iter().map(|x| *x).fold_first(|(mx,my,mz), (x,y,z)| {
		(cmp::min(mx,x), cmp::min(my,y), cmp::min(mz,z))
	}).unwrap();
	let mut max = state.iter().map(|x| *x).fold_first(|(mx,my,mz), (x,y,z)| {
		(cmp::max(mx,x), cmp::max(my,y), cmp::max(mz,z))
	}).unwrap();

	fn neighbours((x,y,z): (i32,i32,i32)) -> [(i32,i32,i32); 26] {
		let mut r = [(0,0,0);26];
		
		let mut i = 0;
		for dx in -1..=1 {
			for dy in -1..=1 {
				for dz in -1..=1 {
					if dx == 0 && dy == 0 && dz == 0 {
						continue;
					}
					r[i] = (x+dx,y+dy,z+dz);	
					i += 1;
				}
			}
		}

		r
	}

	for _ in 0..6 {
		let mut next = HashSet::new();
		for x in (min.0-1)..=(max.0+1) {
		for y in (min.1-1)..=(max.1+1) {
		for z in (min.2-1)..=(max.2+1) {
			let a = (x,y,z);
			let active_neighbours = neighbours(a).iter().filter(|b| state.contains(b)).count();
			if state.contains(&a) {
				if active_neighbours == 2 || active_neighbours == 3 {
					next.insert(a);
					min = (cmp::min(min.0,x), cmp::min(min.1,y), cmp::min(min.2,z));
					max = (cmp::max(max.0,x), cmp::max(max.1,y), cmp::max(max.2,z));
				}
			} else {
				if active_neighbours == 3 {
					next.insert(a);
					min = (cmp::min(min.0,x), cmp::min(min.1,y), cmp::min(min.2,z));
					max = (cmp::max(max.0,x), cmp::max(max.1,y), cmp::max(max.2,z));
				}
			}
		}
		}
		}

		state = next;
	}

	state.len()
}

fn p2(input: HashSet<(i32,i32,i32)>) -> usize {
	let mut state = input.into_iter().map(|(x,y,z)| (x,y,z,0)).collect::<HashSet<_>>();

	let mut min = (0,0,0,0);
	let mut max = (0,0,0,0);

	for (x,y,z,w) in state.iter().map(|x| *x) {
		min = (cmp::min(min.0,x), cmp::min(min.1,y), cmp::min(min.2,z), cmp::min(min.3,w));
		max = (cmp::max(max.0,x), cmp::max(max.1,y), cmp::max(max.2,z), cmp::max(max.3,w));
	}

	fn neighbours((x,y,z,w): (i32,i32,i32,i32)) -> [(i32,i32,i32,i32); 80] {
		let mut r = [(0,0,0,0);80];
		
		let mut i = 0;
		for dx in -1..=1 {
		for dy in -1..=1 {
		for dz in -1..=1 {
		for dw in -1..=1 {
			if dx == 0 && dy == 0 && dz == 0 && dw == 0 {
				continue;
			}
			r[i] = (x+dx,y+dy,z+dz,w+dw);
			i += 1;
		}
		}
		}
		}

		r
	}

	for _ in 0..6 {
		let mut next = HashSet::new();
		for x in (min.0-1)..=(max.0+1) {
		for y in (min.1-1)..=(max.1+1) {
		for z in (min.2-1)..=(max.2+1) {
		for w in (min.3-1)..=(max.3+1) {
			let a = (x,y,z,w);
			let active_neighbours = neighbours(a).iter().filter(|b| state.contains(b)).count();
			if state.contains(&a) {
				if active_neighbours == 2 || active_neighbours == 3 {
					next.insert(a);
					min = (cmp::min(min.0,x), cmp::min(min.1,y), cmp::min(min.2,z), cmp::min(min.3,w));
					max = (cmp::max(max.0,x), cmp::max(max.1,y), cmp::max(max.2,z), cmp::max(max.3,w));
				}
			} else {
				if active_neighbours == 3 {
					next.insert(a);
					min = (cmp::min(min.0,x), cmp::min(min.1,y), cmp::min(min.2,z), cmp::min(min.3,w));
					max = (cmp::max(max.0,x), cmp::max(max.1,y), cmp::max(max.2,z), cmp::max(max.3,w));
				}
			}
		}
		}
		}
		}

		state = next;
	}

	state.len()
}


fn parse(input : &str) -> Result<HashSet<(i32,i32,i32)>> {
	let mut m = HashSet::new();

	let mut x = 0;
	let mut y = 0;
	for c in input.trim().chars() {
		match c {
			'\n' => {
				x = 0;
				y += 1;
			},
			'.' => {
				x += 1;
			},
			'#' => {
				m.insert((x,y,0));
				x += 1;
			},
			_ => Err(Report::msg("bad char"))?,
		};
	}

	Ok(m)
}

const EXAMPLE : &str = "
.#.
..#
###
";

#[test]
fn test_example_p1() -> Result<()> {
	println!("{:?}", parse(EXAMPLE)?);
	assert_eq!(p1(parse(EXAMPLE)?), 112);
	Ok(())
}

#[test]
fn test_example_p2() -> Result<()> {
	assert_eq!(p2(parse(EXAMPLE)?), 848);
	Ok(())
}

const INPUT : &str = "
..##.##.
#.#..###
##.#.#.#
#.#.##.#
###..#..
.#.#..##
#.##.###
#.#..##.
";

