#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;

fn main() -> Result<()> {
	println!("p1: {}", p1(parse(INPUT)?));
	println!("p2: {}", p2(parse(INPUT)?));
	Ok(())
}

fn p1(input: Vec<Action>) -> i32 {
	let mut f = E;
	let mut x = 0;
	let mut y = 0;

	let mut sail = |d: Dir, n: i32| match d {
		N => y += n,
		S => y -= n,
		E => x += n,
		W => x -= n,
	};

	for a in input {
		match a {
			D(d,n) => sail(d,n),
			F(n) => sail(f,n),
			R(n) => f = rotate(f,n),
		}
	}

	x.abs() + y.abs()
}

fn p2(input: Vec<Action>) -> i32 {
	let mut x = 0;
	let mut y = 0;
	let mut wx = 10;
	let mut wy = 1;

	for a in input {
		match a {
			F(n) => {
				x += n*wx;
				y += n*wy;
			},
			D(d,n) => match d {
				N => wy += n,
				S => wy -= n,
				E => wx += n,
				W => wx -= n,
			},
			R(n) => {
				(wx,wy) = rotate2((wx,wy), n);
			},
		}
	}

	x.abs() + y.abs()
}

#[derive(PartialEq,Debug,Copy,Clone)]
enum Dir {
	N,S,E,W,
}
use Dir::*;

#[derive(PartialEq,Debug,Copy,Clone)]
enum Action {
	D(Dir,i32),
	F(i32),
	R(i32),
}
use Action::*;

fn parse(input: &str) -> Result<Vec<Action>> {
	let lines = Regex::new(r"([^\s])(\d+)")?;
	lines.captures_iter(input)
			.map(|x| (x.get(1).unwrap().as_str(), x.get(2).unwrap().as_str()))
			.map(move |(c,n)| n.parse().map(|n| (c,n)).map_err(Report::from))
			.map_results(move |(c,n)| match c {
				"N" => D(N,n),
				"S" => D(S,n),
				"E" => D(E,n),
				"W" => D(W,n),

				"F" => F(n),

				"L" => R(n/90),
				"R" => R(-n/90),
				_ => panic!("parse error"),
			})
			.collect()
}

fn rotate(d: Dir, n: i32) -> Dir {
	let a = match d {
		E => 0,
		N => 1,
		W => 2,
		S => 3,
	};
	match ((a+n)%4+4)%4 {
		0 => E,
		1 => N,
		2 => W,
		3 => S,
		_ => panic!("invalid rotate?"),
	}
}

fn rotate2((x,y): (i32,i32), n: i32) -> (i32,i32) {

	let cos = match ((n%4)+4)%4 {
		0 => 1,
		1 => 0,
		2 => -1,
		3 => 0,
		_ => panic!("invalid rotate?"),
	};

	let sin = match ((n%4)+4)%4 {
		0 => 0,
		1 => 1,
		2 => 0,
		3 => -1,
		_ => panic!("invalid rotate?"),
	};

	(x*cos + y*-sin, x*sin + y*cos)
}

#[test]
fn test_parse1() -> Result<()> {
	assert_eq!(parse(EXAMPLE)?, vec![F(10), D(N,3), F(7), R(-1), F(11)]);
	Ok(())
}

#[test]
fn test_example1() -> Result<()> {
	assert_eq!(p1(parse(EXAMPLE)?), 25);
	Ok(())
}

#[test]
fn test_example2() -> Result<()> {
	assert_eq!(p2(parse(EXAMPLE)?), 286);
	Ok(())
}

#[test]
fn test_rotate() -> Result<()> {
	assert_eq!(rotate(E,1), N);
	assert_eq!(rotate(S,1), E);
	assert_eq!(rotate(E,-1), S);
	assert_eq!(rotate(W,-2), E);
	assert_eq!(rotate(W,-4), W);
	Ok(())
}

#[test]
fn test_rotate2() -> Result<()> {

	assert_eq!(rotate2((1,0),1),(0,1));
	assert_eq!(rotate2((0,1),1),(-1,0));
	assert_eq!(rotate2((-1,0),1),(0,-1));
	assert_eq!(rotate2((0,-1),1),(1,0));

	assert_eq!(rotate2((2,1),1), (-1,2));
	assert_eq!(rotate2((-2,1),1), (-1,-2));
	assert_eq!(rotate2((-1,-2),-1), (-2,1));

	assert_eq!(rotate2((1,1),1),(-1,1));
	assert_eq!(rotate2((-1,1),1),(-1,-1));
	assert_eq!(rotate2((-1,-1),1),(1,-1));
	assert_eq!(rotate2((1,-1),1),(1,1));

	assert_eq!(rotate2((1,1),-1),(1,-1));
	assert_eq!(rotate2((1,-1),-1),(-1,-1));
	assert_eq!(rotate2((-1,-1),-1),(-1,1));
	assert_eq!(rotate2((-1,1),-1),(1,1));

	Ok(())
}

const EXAMPLE : &str = "
F10
N3
F7
R90
F11
";

const INPUT: &str = "
E1
S4
W5
N5
W2
L90
E5
R90
F73
E1
N3
E5
R180
E1
F26
N5
W5
R180
E5
F50
E4
F63
S2
F24
R90
E2
L90
N2
F94
S4
F70
N3
F60
L90
W5
R90
E2
R90
F38
R180
S3
L90
R90
S1
L180
W3
F23
N4
R90
N1
E3
S3
F26
S5
E1
E2
S3
E5
N2
E2
L90
F9
S4
F77
F70
F92
N1
W4
R90
S3
F56
N2
F21
R90
E5
W5
F87
E1
L90
N3
W4
N4
F86
L90
W2
E2
F94
S1
E5
L90
W5
N4
W4
S2
W4
L180
F29
R180
E2
F12
S3
W4
F48
L90
W4
L90
F69
N1
F51
L270
F62
W2
R90
W2
F5
L90
F62
E3
N3
W5
R90
E3
L90
F6
E1
W5
F72
R180
E5
L180
F38
E1
N5
R90
F98
N4
F69
W3
R270
N4
F53
E3
N5
R180
F2
R90
L90
F38
N3
E1
F100
E3
L90
N5
F74
F38
W2
F8
N4
E3
F84
L270
S1
F38
S3
F92
L90
F12
S3
F83
E5
R90
E1
N3
F70
N4
F47
N5
L90
F29
R90
S2
W5
F20
R90
E3
F58
N2
E1
F2
R90
E5
F9
E3
L180
E4
F11
E5
L90
F64
W1
F13
R90
W4
R90
N1
E3
F29
S5
W3
N3
W3
L90
F100
E2
F93
R90
F34
N1
F51
R90
E2
R90
W4
L90
E3
R180
W1
W4
S2
F88
S3
W3
N4
E5
S4
E2
F80
R90
S5
R90
F46
E1
N2
L90
F52
F92
W4
R90
S5
F21
N2
F37
S1
L180
N4
E1
L90
F17
L90
F95
E5
R180
W2
L90
W4
R180
S2
W1
E2
N2
F88
E3
R180
W4
L180
E2
S2
R90
S5
L90
S1
L90
N4
W4
F52
S5
F23
W2
R90
F100
W1
N3
L90
E1
F8
L90
F97
N1
F46
N3
F10
W3
R180
N5
L90
F85
N5
L90
E1
F29
L90
E3
W3
S5
L270
W1
N3
E3
F87
R180
F15
S1
R180
N1
F37
E3
R90
E1
N3
L90
F73
R90
F15
F6
L180
S5
F8
S2
R90
W3
N1
L180
F87
N4
R180
N5
L90
W3
N5
R90
E4
F35
E1
L90
S1
E1
F58
E1
W3
S1
L270
F65
W3
F27
S2
F68
R90
E5
F80
R90
W5
R180
S5
L90
W1
F63
E2
R270
S2
R90
L90
E4
R90
S3
R270
L180
F78
W4
R90
W1
F33
N1
F1
F79
R90
W5
S5
N3
L270
E1
R180
S1
N1
W2
R90
S4
R270
F49
F39
E2
F6
N5
R90
N2
E3
L90
F30
L180
W2
L90
F39
L270
S3
F89
N2
R90
S5
F95
W2
L90
S2
L180
S3
F74
L90
S2
W1
S3
E1
R90
S5
L180
N3
L90
F52
W1
L90
F79
N4
F19
R180
E3
R270
F52
L180
E5
R270
E5
N3
W4
F57
S5
R180
N2
L180
W4
L90
W4
L90
F98
S4
E3
F22
N3
W5
F9
E3
F25
R180
F76
N1
F39
R90
F81
N5
F73
W2
F80
N3
R90
W3
F63
R270
E1
R90
W2
L90
S5
R90
F5
N1
F98
F20
E4
L90
F69
S3
L180
L90
F99
L90
F52
L90
F29
E5
N4
R180
R90
E2
R90
S5
E3
L270
F77
E2
F8
W1
N4
F38
S4
W4
S3
L90
E3
N4
R90
F99
S3
E2
F80
N5
F43
N3
F99
L90
S4
F66
L90
L90
N2
F80
N3
F86
S4
R270
S3
E4
F83
E2
R90
F26
E3
N5
E2
S4
F60
E5
S3
R90
W4
S5
E2
R90
F67
N1
E3
N2
L90
E4
L90
W5
E1
R90
S2
W1
N1
F39
F23
F64
R180
F82
S4
W1
F18
F57
W5
F55
L90
N5
L90
W2
L90
F12
E4
R90
W4
F37
W3
F72
R90
N5
R90
F95
W3
L180
F6
W5
F67
R90
F60
R90
F77
E4
R180
F81
E1
F39
E2
N2
F95
N5
F69
L90
N3
R90
S1
F11
N1
L180
E2
F47
W2
L90
N2
W3
N2
R180
F20
L90
F17
S3
E4
L270
W4
S2
L180
W1
S1
R180
N1
W4
N3
L180
N4
F30
E5
R90
S2
F62
L90
E5
F17
L90
W2
R180
E5
S2
F8
R180
S5
F56
S1
E3
F91
L90
E1
R90
S3
R90
E3
R90
S1
E2
F56
E5
L90
S4
F44
S2
L90
F38
E3
S5
E5
S5
W3
N1
R90
N5
F82
W2
S3
R90
E4
R90
E5
E1
F64
N2
L180
S1
E2
N4
L90
F7
R180
S3
F79
W4
S2
L180
E5
R180
N1
L90
E2
R90
F92
S4
F42
E4
R180
W3
R90
N3
F9
L90
W5
F4
R90
S4
F39
N3
F78
E5
N2
F79
S3
R90
N2
F2
S1
F94
N2
W2
F17
R90
W1
S3
E2
F83
L180
W2
N3
R270
N5
W4
F40
";

