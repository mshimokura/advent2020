#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::RangeInclusive;
use std::iter;
use std::cmp;

fn main() {
	println!("p1: {}", p1(parse(INPUT)));
	println!("p2: {}", p2(parse(INPUT)));
	// 2439 (9 monsters) == too high
}

fn p1(input: Vec<(i64, Vec<Vec<Pixel>>)>) -> i64 {
	let image = dfs(&build_edges(&input), &input.iter().map(|(x,_)| *x).collect::<Vec<_>>(), Vec::new()).unwrap()
		.iter()
		.map(|row| row.iter().map(|(id, _offset)| *id).collect::<Vec<_>>())
		.collect::<Vec<_>>();
	image[0][0] * image.iter().last().unwrap()[0] * image[0].iter().last().unwrap() * image.iter().last().unwrap().iter().last().unwrap()
}

fn p2(input: Vec<(i64, Vec<Vec<Pixel>>)>) -> usize {
	let layout = dfs(&build_edges(&input), &input.iter().map(|(x,_)| *x).collect::<Vec<_>>(), Vec::new()).unwrap();
	let size = input[0].1.len();
// remove edges
	// concatenate to one grid
	
	let mut image = Vec::new();
	{
		for (ly, row) in layout.iter().enumerate() {
			for (_lx, (id, offset)) in row.iter().enumerate() {
				iter_rotate(input.iter().find_map(|(i,img)| if i == id { Some(img) } else { None }).unwrap(), *offset, |tx,ty,p| {
					if tx == 0 || tx == size-1 || ty == 0 || ty == size-1 {
						return;
					}
					let y = ly*(size-2) + (ty-1);
					if y >= image.len() { image.push(Vec::new()); }
					image[y].push(p);
				});
			}
		}
	}

	/*{
		let mut py = 100;
		iter_rotate(&image, 0, |x,y,p| {
			if y != py {
				println!("");
				py = y;
				if y % size == 0 { println!("{:?}: ", layout[y/size][x/size]); }
			}
			if x % size == 0 { print!(" "); }
			print!("{}", p);
		});
		println!("");
	}*/

	// find sea monsters
	let monster_str = "
                  #
#    ##    ##    ###
 #  #  #  #  #  #
";
	let monster_pattern = {
		let mut r = Vec::new();

		let mut x = 0;
		let mut y = 0;
		for c in monster_str.chars() {
			if c == '#' {
				r.push((x,y-1));
			}

			if c == '\n' {
				y += 1;
				x = 0;
			} else {
				x += 1;
			}
		}

		r
	};

	let num_monsters = {
		let mut total_count= 0;
		for offset in 0..8 {
			let mut count = 0;
			let mut monsters = HashSet::new();
			
			iter_rotate(&image, offset, |x1,y1,_| {
				let mut pat = monster_pattern.iter();
				let mut current = *pat.next().unwrap();
				iter_rotate(&image, offset, |x2,y2,p| {
					if x2 < x1 || y2 < y1 {
						return
					}
					if current != (x2-x1,y2-y1) {
						return
					}
					if p == Off {
						return // do not update current, this "breaks"
					}
					match pat.next() {
						None => {
							count += 1;
							for (dx,dy) in monster_pattern.iter() {
								monsters.insert((x1+dx,x2+dy));
							}
						},
						Some(x) => {
							current = *x;
						},
					}
				});
			});

			if count > 0 {
				total_count += count;
				let mut py = 0;
				iter_rotate(&image, offset, |x,y,p| {
					if y != py {
						py = y;
						println!("");
					}
					if monsters.contains(&(x,y)) {
						print!("O");
					} else if p == On {
						print!("#");
					} else {
						print!(".");
					}
				});
			}
		}
		
		total_count
	};

	println!("sea monsters: {}", num_monsters);

	// return number of 'On' pixels - number of sea monsters * number of On pixels in a sea monster
	let monster_pixels = monster_pattern.len();
	image.iter().flat_map(|row| row.iter()).filter(|x| **x == On).count() - num_monsters * monster_pixels
}

// iterate an image with the specified rotation
fn iter_rotate<F> (image: &Vec<Vec<Pixel>>, offset: usize, mut f:F) where F: FnMut(usize,usize,Pixel) {
	let size = image.len(); // rows == columns == size
	for y in 0..size {
		for x in 0..size {
			let mut ix = x;
			let mut iy = y;
			
			// 'reverse rotate' 0->x, 0->y vectors
			if offset == 1 {
				(ix,iy) = (size-1-iy, ix);
			} else if offset == 2 {
				(ix,iy) = (size-1-ix, size-1-iy);
			} else if offset == 3 {
				(ix,iy) = (iy, size-1-ix);
			}
			
			// flipped
			else if offset == 4 {
				ix = size-1-ix;
			} else if offset == 5 {
				(ix,iy) = (size-1-iy, size-1-ix);
			} else if offset == 6 {
				(ix,iy) = (ix, size-1-iy);
			} else if offset == 7 {
				(ix,iy) = (iy, ix);
			}

			f(x,y,image[iy][ix]);
		}
	}
}

#[test]
fn test_iter_rotate() {
	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 0, |x,y,p|
		if x == 2 && y == 0 { assert_eq!(On, p); } else { assert_eq!(Off, p); });
	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 1, |x,y,p|
		if x == 0 && y == 0 { assert_eq!(On, p); } else { assert_eq!(Off, p); });
	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 3, |x,y,p|
		if x == 2 && y == 2 { assert_eq!(On, p); } else { assert_eq!(Off, p); });
	iter_rotate(&vec![vec![Off,Off,On,On],vec![Off,Off,Off,Off],vec![Off,Off,Off,Off],vec![Off,Off,On,Off]], 6, |x,y,p|
		if y == 3 && (x == 2 || x == 3) || (y == 0 && x == 2) { assert_eq!(On, p); } else { assert_eq!(Off, p); });

	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 7, |x,_y,p|
		{ print!("{}", p); if x == 2 { println!("") } });
	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 7, |x,y,p|
		if x == 0 && y == 2 { assert_eq!(On, p); } else { assert_eq!(Off, p); });

	iter_rotate(&vec![vec![Off,Off,On],vec![Off,Off,Off],vec![Off,Off,Off]], 5, |x,y,p|
		if x == 2 && y == 0 { assert_eq!(On, p); } else { assert_eq!(Off, p); });
}

fn build_edges(input: &Vec<(i64, Vec<Vec<Pixel>>)>) -> HashMap<i64, Vec<Vec<Pixel>>> {
	let mut edges = HashMap::new();
	for (id, rows) in input {
		let left = rows.iter().map(|row| row[0]).rev().collect::<Vec<_>>();
		let right = rows.iter().map(|row| *row.iter().last().unwrap()).collect::<Vec<_>>();
		let top = rows[0].to_vec();
		let bottom = rows.iter().last().unwrap().iter().map(|x| *x).rev().collect::<Vec<_>>();
		edges.insert(*id, vec![right, top, left, bottom]);
	}
	edges
}

fn get_edge<'a>(edges: &'a Vec<Vec<Pixel>>, offset: usize, index: usize) -> Box<dyn DoubleEndedIterator<Item = &Pixel> + 'a> {
	if offset >= 4 {
		Box::new(edges[(8-offset+index + ((index+1)%2)*2 )%4].iter().rev())
	} else {
		Box::new(edges[(8-offset+index)%4].iter())
	}
}

fn dfs(edges: &HashMap<i64, Vec<Vec<Pixel>>>, ids: &Vec<i64>, image: Vec<Vec<(i64, usize)>>) -> Option<Vec<Vec<(i64, usize)>>> {
	
	// find first spot that needs to be filled, fill it, then call dfs().
	// if dfs() returns None, continue.
	// if empty spot has nothing, return None
	let size = (edges.keys().count() as f32).sqrt().round();
	for (i, row) in image.iter().enumerate() {
		if (row.len() as f32) < size {
			for id in ids {
				for offset in &[0,1,2,3,4,5,6,7] {

					// check if edges[id], offset matches
					let tile = edges.get(&id).unwrap();
					if i > 0 { // top
						let (prev_tile_id, prev_offset) = image[i-1][row.len()];
						let prev_tile = edges.get(&prev_tile_id).unwrap();
						if !get_edge(prev_tile, prev_offset, 3).eq(get_edge(tile, *offset, 1).rev()) {
							continue;
						}
					}
					if row.len() > 0 { // left
						let (prev_tile_id, prev_offset) = row.iter().last().unwrap();
						let prev_tile = edges.get(&prev_tile_id).unwrap();
						if !get_edge(prev_tile, *prev_offset, 0).eq(get_edge(tile, *offset, 2).rev()) {
							continue;
						}
					}

					let mut next = image.to_vec();
					let mut next_row = row.to_vec();
					next_row.push((*id, *offset));
					next[i] = next_row;
					let next_ids = ids.iter().map(|x| *x).filter(|x| x != id).collect();
					match dfs(edges, &next_ids, next) {
						Some(x) => return Some(x),
						_ => (),
					}
				}
			}

			return None
		}
	}

	// try next row
	if (image.len() as f32) < size {
		let mut next = image.to_vec();
		next.push(Vec::new());
		return dfs(edges, ids, next);
	}

	// if nothing needs to be filled up, just return image
	Some(image)
}

#[derive(PartialEq, Copy, Clone)]
enum Pixel {
	On,
	Off,
}
use Pixel::*;

impl std::fmt::Debug for Pixel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if *self == On {
			f.write_str("#")
		} else {
			f.write_str(".")
		}
    }
}
impl std::fmt::Display for Pixel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if *self == On {
			f.write_str("#")
		} else {
			f.write_str(".")
		}
    }
}


fn parse(input: &str) -> Vec<(i64, Vec<Vec<Pixel>>)> {
	let r = Regex::new(r"Tile (\d+):\n(([#.]+\n)+)").unwrap();
	r.captures_iter(input)
		.map(|c| {
			let id = c.get(1).unwrap().as_str().parse().unwrap();
			let rows = c.get(2).unwrap().as_str().trim().split("\n")
				.map(|line| line.chars()
					.map(|c| match c {
						'#' => On,
						'.' => Off,
						_ => panic!("unknown char: {:?}", c),
					})
					.collect()
				)
				.collect();

			(id, rows)
		})
		.collect()
}
#[test]

fn test_example_parse() {
	let input = parse(EXAMPLE);
	println!("{:?}", &input);
	assert_eq!(10, input[1].1[0].len());
	assert_eq!(10, input[1].1.len());
}

#[test]
fn test_example_p1() {
	assert_eq!(20899048083289, p1(parse(EXAMPLE)));
}

#[test]
fn test_example_p2() {
	assert_eq!(273, p2(parse(EXAMPLE)));
}

#[test]
fn test_simple_p1() {
	assert_eq!(1*2*3*4, p1(parse("
Tile 1:
.#
#.

Tile 2:
#.
.#

Tile 3:
.#
#.

Tile 4:
#.
.#
")));
}

const EXAMPLE : &str = "
Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...
";

const INPUT : &str = "
Tile 1487:
.##..#...#
##.##.....
...#....##
##..#..#.#
.####....#
.#.#..#..#
..##.....#
..##....##
#.....#..#
...#..####

Tile 3637:
##...#....
#.#..#...#
....##...#
#.........
#......#.#
##...#.#..
#.........
##.....###
##.#......
##.#.##..#

Tile 3433:
.....#..#.
##..##...#
....#.....
.........#
#........#
#..##..###
#..#..##..
#...##.#..
.#...#..#.
#.##....#.

Tile 3931:
#....##..#
...#..###.
#...#..#.#
.......#..
#.......#.
###......#
###.#.#..#
.##..#..#.
...##..#.#
..##..#.#.

Tile 2213:
.###...##.
..##......
..........
.##...#...
...##...#.
..#.#..#.#
..##......
#..#.#.###
...#......
#.#..##...

Tile 1901:
.#.##.#.#.
#.........
#.##..#.##
...###...#
.....#.#.#
.#.....#..
....#.#...
..#...#..#
#........#
####.###..

Tile 3917:
.##.#..#.#
#.##....#.
#.....#...
#.#...#.##
..........
..........
.#....#.#.
.........#
.....#....
#.#.##....

Tile 1831:
.#..#..##.
#.....##..
###....###
.#.#.##..#
#....#...#
......#.#.
.....#..##
#..##.....
####.#.##.
...##.#..#

Tile 3557:
..#.#.#.#.
##........
.......#..
.##.......
..........
....#..#..
#...#....#
#..#####.#
#..#...##.
#.....###.

Tile 2659:
####.##.#.
.##.##...#
####..#...
##........
####......
.......#.#
###....#..
.....#..##
#....#...#
.#..###.#.

Tile 3061:
.#..#.#..#
........#.
##.#....#.
###....#..
.........#
..#..#....
....#..#.#
.#.##...##
##.#....##
.#.#.##.##

Tile 3593:
###.#.####
.#........
#..#....#.
.......###
.#....##.#
....#..##.
#.#.....##
#..#...#..
.#......#.
#....##..#

Tile 2719:
..###.#..#
..##...##.
..##.....#
#.......#.
...#....##
#.#....#..
#.......#.
#..#.....#
........##
.#.#..##.#

Tile 3089:
##.#.#.#.#
.#..#....#
.#.#.....#
##...#.#.#
.......#..
###..####.
....#.....
........#.
.#.....##.
..####..#.

Tile 1723:
..#.##.###
#..###.#.#
..#..#...#
....#.#...
....#....#
#....#....
#.....#...
#.#..#.#..
#...###..#
###.####..

Tile 2297:
.##....#.#
####..#.##
#.##...##.
#.##...#.#
###.##..##
....#...##
##....##.#
..##...##.
##.#.#..#.
#...#.##..

Tile 3659:
.#####....
....#.#.##
.#..#....#
....#....#
.....#...#
.....#..##
##..#.....
##...#....
####....##
##......#.

Tile 1847:
###.#.####
..##.....#
#.......#.
##......#.
..#.#.....
#.......#.
.#..#...#.
.#......#.
#.....###.
##.###.##.

Tile 1601:
#.#.##.###
.#..#...#.
##...#..#.
#.#####..#
.......#.#
##..##.##.
..#.#..#..
..###....#
.....#...#
....#####.

Tile 3323:
..#..##.##
##......##
###.#...#.
.#..#..#..
#...#..###
#....##...
##.....#.#
...####...
....#.....
..##....##

Tile 1321:
###.######
#...#.#..#
##........
#..#..#.#.
..##.#..#.
.........#
.#.......#
#..#......
##.#.....#
.###...#.#

Tile 2551:
....##...#
...#.##...
.##.......
...#..#...
#.#...##.#
#..##.#...
.##..#.##.
##....#..#
...#.....#
..#.#.#.#.

Tile 2503:
...#..##.#
.........#
#.#....##.
#.........
##..#.#...
#...#..#..
...#......
.##.##....
#.........
###.....##

Tile 1163:
#.####..#.
.##.#.#...
#.#..#####
......#...
##....#..#
#.......##
#.#.##..#.
##...###.#
#.........
.##.#...##

Tile 1039:
#.###.#.#.
.##......#
.#...#..#.
.#...#...#
#.#...#..#
##.#...#..
#.##......
#.#.#..#..
##...#..##
#.####..##

Tile 2953:
...#.#.#.#
..#.#.....
.........#
#..#...#.#
......#...
.........#
....#..#..
.###....#.
##.##..###
..####.#..

Tile 1609:
...#...#..
#....##..#
...###.#..
#.##.##..#
#.#.##.#..
##.......#
.##...#..#
#....#....
........#.
.....#..#.

Tile 1187:
####.##...
..........
.....#...#
#.#......#
#....#.###
.#..###.#.
##...#....
......#..#
....#.#..#
#####...##

Tile 3313:
#######..#
#.#...####
..........
#.#..##...
###......#
##........
##......##
##..#.#...
..#..#..#.
#..#...##.

Tile 3221:
....#.#.#.
##..#.....
.#.##..#..
#..##..#.#
....#....#
##.......#
#.......#.
#.###...##
#..#......
#....##...

Tile 2647:
.##.#.####
..#.......
#...##....
#..#....##
......#...
..##......
.#.....#.#
###.##....
###...###.
##.#.#..#.

Tile 2879:
#.#.#..###
......####
##....#.#.
.#.......#
....##..#.
#..#.###.#
#.........
##.......#
#...##...#
..#...##.#

Tile 2423:
.###.#..#.
..###.##..
..#.#...##
#....#...#
#..#.##...
#..#....##
.....#....
##.##....#
....#....#
...#..#.#.

Tile 2557:
.#..#..##.
#....##.##
...#.#...#
#...####..
#........#
...#....##
..........
.##..#.##.
#.#..#...#
.###..#.#.

Tile 2819:
..#......#
#....#....
##..#...##
#......###
#..##.....
#.###.#...
.##...####
..........
#........#
#...##.#.#

Tile 2531:
....#.#..#
..#..#..#.
#....#...#
#..#..#..#
.........#
#.#.......
#......###
#.#...##..
#....###.#
.#.#####.#

Tile 1109:
..#.#.#.##
.....#....
#...##.#.#
##.##.#...
.........#
.#..#.#..#
#.........
.##...#..#
#.#..#..#.
.#.###.#..

Tile 1103:
#..##.#...
.###......
#..#.....#
.....###.#
.#..#.##.#
......##.#
#...##.#..
...#...###
.##..#.##.
.#.#.#..#.

Tile 2713:
.###.##...
#.##...#.#
.....#...#
#.........
##...#..##
.###.#.##.
#.#..##.##
#...#..##.
...#...##.
.....#.##.

Tile 2927:
##..#...#.
...#..#..#
#..#..#...
.....#.#..
##.......#
...##.##.#
#.#.......
.###....#.
....#.####
####.###.#

Tile 3319:
....#.#.##
#.###..###
.#....#.#.
.....#..#.
.##....#..
....#...##
#.#...#...
.......#..
#.#.###.#.
......##..

Tile 2311:
#.#.##...#
..#....#..
..#..#..#.
..#.#.#.##
......####
..#..#..##
##....##.#
.#..#.....
.....##..#
...##.....

Tile 1549:
##.#.#.#.#
##..#...#.
.#....#...
.#....##.#
..#..###.#
#....#..##
.#.......#
#.....#.##
.....#....
#.#...#.#.

Tile 3631:
...#.###.#
........#.
###..##..#
###.....#.
##....##.#
....#.....
###.#.#...
...#..#...
##..#....#
....##..#.

Tile 1787:
#.##.##.#.
....#...#.
#..#..#..#
...#.....#
#.##.#....
##.......#
#.....#...
#...##.#..
.#.##.#..#
###.#....#

Tile 3467:
########.#
##...#.#.#
...#......
.....#.#.#
#.....#..#
#........#
...#......
..#.....##
.#.##..#..
..#.....#.

Tile 3373:
##..#.#..#
...#......
###.##..#.
#........#
.#...#...#
#....##...
.#.....#..
.#....#..#
#.#..####.
...####.#.

Tile 3911:
#.###.#..#
.##......#
...#......
#...#..##.
.......#.#
#.#......#
...##..#.#
##..#..#.#
#......##.
#####..###

Tile 1327:
...#.#..#.
.........#
#..#...#..
#........#
###.#.....
.........#
##.#......
#........#
#..#...#.#
.#.##..##.

Tile 3643:
.#.#...#.#
#.##....#.
#..#...#..
....##....
#....##.##
..........
#...#.#.##
##.#..#..#
...#....##
..##..#..#

Tile 2447:
#...#.###.
#.....#..#
#.#.......
###..#...#
.....##.##
#....#...#
#.#.#.....
#.....#...
...##.....
.#.###.##.

Tile 3181:
.###..#...
##...#.#.#
#..#......
#......#.#
.#...##.#.
#.#......#
.........#
.........#
#.#...#...
##.##.#.#.

Tile 3037:
#########.
...#.#...#
.##......#
.#........
#....#...#
#..#..#.#.
...###.#.#
#.#...#.##
##..#.#..#
#..#.##.#.

Tile 3671:
##.#.#....
........#.
##.#....#.
#.##.#.#.#
##....##..
.##...#.#.
.#.#...#..
##...##..#
#.#....###
##.#..###.

Tile 2909:
.##...###.
#.....#..#
.#..#.....
#.###.#.##
#.##.#.##.
..#......#
.#..#.#...
#..#..#.#.
.#.#...#..
.###..##.#

Tile 3571:
..#.#.#..#
#....#....
.....##..#
.....###.#
....#.#...
......#..#
#.......##
..........
..#.##..#.
##.##.##.#

Tile 1931:
###.##...#
#...#.#...
#..#..#.##
..##......
...##....#
#.#.##..##
#.##.....#
#..#......
..#..#.#.#
....##.#.#

Tile 2689:
#....#.##.
....#...##
......##..
#....#...#
........#.
..........
##.#.###..
..#.......
....##....
...#.#..##

Tile 3499:
.#.###.#.#
......#..#
..#......#
.#......#.
##.......#
##...##...
.#......#.
#......#..
#....#....
###.######

Tile 1277:
...##.#...
......#..#
###......#
#.........
..##.....#
#.#....###
#.........
#..#.....#
#.........
###.#.#...

Tile 2633:
.#..#.##..
#........#
......#..#
..#.......
.##....#.#
#.......#.
.#.##.....
#.........
........#.
##.#.#####

Tile 1699:
.##.#..##.
#..##.....
...#...##.
..#....#..
#..#..##.#
..#.....##
.##......#
..#....#..
#..###..#.
#..#.####.

Tile 2339:
.#.#####..
##.#...#..
..##.#....
.###....#.
#..##....#
#.........
.....#....
#...#....#
#..##...#.
#....##...

Tile 1993:
.####.....
.....#..#.
..#.#..#.#
#........#
...##.#.#.
..#.#.#..#
.###.##.#.
..#.#..#..
....#####.
..#.#...##

Tile 2467:
.####..#.#
#####.....
##...##...
#....##.##
.....###..
#....###.#
......#..#
....##.#..
....##.#..
..#...#.#.

Tile 2153:
.##.##.#.#
.........#
....#..##.
...#......
#..#....##
....#....#
..##.#...#
........#.
##..#.##.#
..#.##....

Tile 1867:
.#....####
#...#....#
....#..#..
.#.....###
....#..#.#
#..#..#..#
....#...#.
.....#...#
.....##...
.##..#.#.#

Tile 2111:
..#.#..###
.....#....
..#.#.##..
.#.......#
..#.......
..#..#.#..
.#.....#..
....#....#
##.#...#..
####.##...

Tile 3307:
.#.#..####
###..#.#..
........##
.....#..#.
.#....##..
###.#....#
.#........
.......#..
#.#.....##
..#.#..##.

Tile 1061:
##.##.....
#..#.#..#.
..#....#..
#.....#..#
#.#..#...#
#.....####
#....#....
##......##
......##.#
##...##...

Tile 1021:
##.###.##.
.#....#..#
#.....#..#
#..#..##.#
##.....#.#
#.#....##.
..#.....#.
.#.....#.#
.#..#.....
.....#.##.

Tile 3677:
.#...#...#
..........
....###...
.######..#
..#...#...
#...#.#...
##.......#
..#......#
.#..#....#
#..##.####

Tile 1231:
#.###.##..
..#.......
#..#.....#
#..#.....#
.#.....#.#
##......##
.#.###....
..#....###
##.....#.#
##.....#..

Tile 3301:
.........#
.#..#....#
##.#...#.#
..#..#...#
.##....##.
..#...#...
##.##....#
#..#.#....
.....#..##
#...#..###

Tile 2861:
.....#.###
.#####.###
###.#...#.
##..###..#
..........
##...#.#..
##..#..#..
.##..#...#
#.....#...
.....###..

Tile 3823:
.##.##.###
..#.#.....
#.#####.#.
.#......##
#.#..#.#.#
...#..##.#
##..##...#
....#..#..
....#....#
##.#......

Tile 2027:
.#.#..#..#
....#.....
.#..##....
#..###..##
#...#.....
#...#...##
.#........
##.##.#...
.....#.#..
###..##.##

Tile 3673:
....####.#
.#...###.#
....#..#..
.#.#.#....
..#..#....
#....##.#.
.....#....
#...#....#
#..#.#....
###.#.#...

Tile 1777:
.####.####
#..#....#.
....###...
.....#.#..
....#.....
.#........
##.#.....#
.#.......#
.#...#...#
.####.#.##

Tile 2609:
.#.#####..
......#..#
#.#.#.....
..##...#..
....#....#
#....#...#
..#.#...##
#...#...##
#..#.....#
###..#....

Tile 2687:
##...#.#.#
.####.....
#.#..#..#.
#...##...#
#....#.##.
.##......#
#..##..#..
.#.#.#..#.
#.#.##..##
##..#...#.

Tile 1451:
######.#..
##.###...#
...##.....
#..##...#.
.....#..#.
#..#..#...
..#...#...
##..##...#
#......##.
..#..#.#..

Tile 3251:
.####..##.
#.........
##..###...
#......#..
##..##..#.
..........
#........#
#........#
..##.#.###
........#.

Tile 3391:
..#######.
#..#.#...#
.......#..
#...#....#
#.....#..#
##........
#.....#..#
.....#..##
..###...##
.#..#.#.#.

Tile 1129:
.##..#.#..
##.#.#....
###...#..#
..........
......####
..........
##...#...#
#.#...#.##
#.........
###...####

Tile 2087:
..###.#...
#...##...#
...#.....#
..##...#..
##.....#.#
####.....#
..#.#..#..
#.#.##....
..###....#
.#.#.##.#.

Tile 1361:
#...######
...#......
#.#.#.#..#
#.....#.##
....#...##
#.......##
#......#..
#.....#..#
..#....#.#
.....##.#.

Tile 1583:
#.#..##.#.
#..#.#....
#...#..#..
##.....#.#
#...#.#...
...###....
..#..#.##.
...####..#
#....#....
#..#.##.#.

Tile 3361:
.###.#####
.##..#...#
.....##...
#.#....#..
#..#...#..
.........#
#........#
..#.......
#.###.....
#...###...

Tile 3463:
.....###.#
...##.#..#
.#...#....
###.##..#.
#.###...#.
#..#..#...
#..##.....
#..#...#.#
#........#
.###..#.#.

Tile 1997:
#.##....#.
..#...#...
#........#
.....#.#.#
.#...#....
#..#.#....
##...#....
##.....#.#
#.........
#.......##

Tile 2131:
.##.##.#..
...#.#...#
###.####.#
......###.
.#.#..#.##
..#..#.###
......#.##
..##..#..#
...#.#....
###.####..

Tile 3943:
###..#####
.#.#...#.#
##..#.#.##
#..#....##
#...#....#
#..#..##.#
..#....##.
#.......##
#.........
..##...##.

Tile 1481:
.###...#.#
####.##.#.
.###.##.#.
...#.#...#
#...#....#
.#.....#..
..##....##
##.##..###
.....#...#
###.##.#.#

Tile 3191:
..#.###.##
#...#...#.
#....#....
..#.....#.
...#..#.##
........##
#..#......
#.........
.#........
.......##.

Tile 3833:
......#...
.......#..
......#.#.
...#.....#
...##.#.#.
.#.....###
#..##..#..
....#....#
.#..#..#..
#.##..#.#.

Tile 1627:
###...####
#.........
#..#..###.
.....##..#
#....#....
#.##..##.#
....#..##.
...##...#.
.###..#...
..#.##.###

Tile 2801:
#####.###.
..#.##.###
.##...##.#
#.......##
.......#..
..........
......#..#
#.....#..#
##..#.###.
.#..#.#.##

Tile 1889:
##.#....##
..###....#
#..#.....#
.#......##
##..#.#...
....#...##
......#...
#.##....#.
...##..#..
.##.#.#...

Tile 1823:
....#.#.#.
.#........
#...##...#
...####..#
...#...###
#...#..#..
#.#..#....
#...#..#.#
......#...
###.###.#.

Tile 2663:
##.##.#.##
.#.#.....#
.#.##....#
#....#...#
#..#..##..
#.###...#.
.......#.#
#..#......
.....#....
.##...#...

Tile 1091:
.##...#.##
#.....#...
####.....#
.......#..
...#......
#.#..#...#
#.#.....##
##......#.
..#.......
.#.#...##.

Tile 3163:
#####..#.#
####..#.#.
.##.#..###
...#....#.
.........#
..#......#
##.....#.#
#.....#..#
#..#...#.#
.####..##.

Tile 1031:
.#.##.#...
##.##..#.#
.....#....
#..#......
......#...
.....#.#..
.....#.#.#
.#...#....
.##....##.
.#.##....#

Tile 2269:
####.####.
#.........
#...##.##.
..##..#..#
...#......
.#....#..#
#..##....#
..........
..#.....#.
.####...##

Tile 3793:
.##.#.###.
.##.##..##
..#...#.#.
......#..#
..####....
#........#
#....#...#
#....#.#..
.#.#..###.
#...#.###.

Tile 3079:
#.#.#.#...
##..#....#
..#...#..#
#..###....
#.#.......
..#.#.....
....#.....
###.##.###
...##.....
...###...#

Tile 3187:
.##.##.###
.#.....#.#
..##.##.#.
#.###.....
.##.##..##
#..#.....#
#...#.#..#
..#.#...##
#........#
...##.#...

Tile 1553:
.###.#..#.
.#..##.#..
#....#...#
#....##..#
#.#.##....
....#...#.
.........#
#..##..#..
.#......#.
.###.#.#..

Tile 1049:
##.#.#.###
...###.#.#
...##.....
#...#.#.#.
....##....
.##....#..
.....#.#.#
..#..#..#.
..#.#....#
....#..##.

Tile 2083:
.#......##
...#......
....#.#...
...##.#..#
#...##...#
.....#...#
..##.#...#
#....#..##
.....#....
...#..#.#.

Tile 3049:
.#...##.#.
.#..#....#
....#.##..
##..#.....
.........#
#......#..
...#.#....
...#.##.#.
.##...#..#
.##.##.#.#

Tile 3167:
..##...###
#....#..#.
#.#..#....
#.........
#..##..#.#
..........
.....#..##
.#.....##.
##......##
.##....#..

Tile 1693:
.#.###..##
#.........
##...#....
..#....#.#
...#......
...#.#...#
.#.....###
.....#####
...##....#
.#.##.....

Tile 2473:
....#.##.#
..#......#
..#......#
....#..#.#
#...#.....
..#..#..#.
.##..###.#
#.#..#....
#....##...
#.#..#.###

Tile 3821:
.#.#####.#
#.......#.
#..#.#..##
##........
.#.....#.#
.....#....
.#.#.....#
..#..##...
##..#.....
##.....###

Tile 3529:
........#.
.#....##.#
#......##.
#..#...#.#
##........
#.#...#...
...#..#.##
.#.#..#..#
#..####..#
.##..##..#

Tile 1801:
#####.#.##
#.#.......
#.#.#####.
##.#.....#
....##.#.#
#..#.#....
.#........
#.#.##...#
#.#......#
..#..#...#

Tile 3709:
.##....#..
........#.
....#.....
##..#.#...
..#..#.#..
..#.#.....
#.........
#.#.##..##
....##....
#.#.#....#

Tile 2707:
###..##.##
.#.####..#
#.#.##.#.#
#...#.#...
.......###
.....##...
...####...
###...#...
.........#
#.###.##..

Tile 3733:
##...#.##.
.....#....
#......##.
##........
##...##...
#.#...#...
#......#.#
#.#....#.#
#...#.#.##
.#..#...#.

Tile 1663:
#.#.#.###.
....##...#
...#...#.#
##...####.
.#........
##....#..#
.....#...#
#..#...#..
#######..#
##.#.#.##.

Tile 2309:
..##..#...
...#.....#
########.#
#....##..#
.#..#....#
..#.#....#
##.....#.#
..........
.#....#.##
##..#.###.

Tile 1483:
..#..#...#
##.#.#....
#.#....###
##..#.####
##.#.#.#.#
..#.#..##.
..##.....#
..#....###
#..##....#
..####..##

Tile 2591:
.#.####.##
.#.#.....#
#..##..#..
#...#.#..#
##.##.#...
.....###..
#....##..#
#....#..#.
##.#..##..
..#..#.#..

Tile 2671:
...#.###.#
#.#.....#.
.#.....#..
...##.####
.#.###..##
.#.#.#.#..
#...##..#.
#.......##
.#....##.#
.#..##..##

Tile 1051:
#.#.#.....
..#..#.##.
......#..#
#.#.......
..#.#.#..#
.#.##.#..#
#..#.###.#
#...#..###
#.......#.
...#...###

Tile 1613:
..##...#.#
#......#..
#.#..#..##
.#.#.#....
#.####.###
##..##.#..
##........
.#.#...#..
#.#....#..
.##...#.##

Tile 3457:
##.#.##..#
##.#...#.#
#..#...#.#
###....#..
.#....#...
#...##..##
..........
.........#
..#..#..#.
..#..####.

Tile 3739:
...######.
#.#.#..##.
#.#.....#.
#..#..#.##
#..#......
#.#......#
#..##.....
#.##.#....
#.#..#..#.
#.#.#..##.

Tile 2069:
....#.###.
#.#...#.#.
.##.......
..#.#.....
..........
#...#..#..
..........
###...#...
#...#.###.
.##.##..##

Tile 3623:
##..##....
#..#.#....
.....#....
#........#
.#.....#..
##..#.#.#.
#....#...#
..........
#.##.....#
#.....#..#

Tile 3613:
..#.##..#.
#...##....
.........#
#......#..
....####.#
..#.##....
..#.#...##
#.#..#....
#.#...#...
.#####.#..

Tile 3329:
##....#.#.
....#..##.
..##..#.#.
.#.##..#..
#.#.#..#..
.........#
.......#..
#..###...#
###.#.#.#.
##.#.#...#

Tile 3209:
.#.....##.
....####..
..........
..##...#..
....#...#.
#...##.#.#
#.##.###..
#...#....#
##........
..#.#####.

Tile 2351:
#....#####
#.....#...
.....#...#
#.#..#....
.#.###...#
.#...##..#
#.#......#
#....##.#.
...#....##
#####...#.

Tile 3011:
#.###....#
#...#.#..#
........##
#.....#...
..#...#.#.
......#.##
.....##..#
.##..#...#
#...#.....
#######.##

Tile 1409:
#..#..#.#.
....#..#..
#...#...##
#..#.#....
...##..#.#
#...#.....
...#.##.##
#..##.....
.#......##
.#######.#

Tile 2341:
.#.....##.
.##..#...#
.#..#.....
#........#
#..#.....#
###.......
##.##.#...
.......#.#
#.#...#...
##.#.##...

Tile 1747:
#.....#...
..##...#.#
...###....
.........#
.#..##.#..
#.##.....#
#.###..#..
#.#......#
##..##....
#.####.#.#

Tile 3881:
...###....
.........#
#...##....
#...##...#
#.#.#.#...
..#..##.#.
#....#....
#....##.##
...#......
####.##.#.

Tile 1307:
#####.##..
#...#..###
..........
##.#.....#
..#.#..##.
....#.#.#.
..........
#.........
.#.....#..
.#.#.####.

Tile 3779:
#.....####
....#...##
#...#...#.
#.....##..
#.##....#.
#..#....##
.......#..
#.#.#....#
.#..#.#...
.##..#....

Tile 2957:
.....##..#
..#....#.#
#..#...##.
.......###
...#..#.##
##......#.
.#.....##.
#...##.##.
#....#.###
..##.#..##
";

