#![allow(dead_code)]

use regex::{Regex,Match};
use std::vec::{Vec};
use eyre::Result;

fn main() -> Result<()> {
	
	let mut p1 = 0;
	let mut p2 : u64 = 1;

	let lines = Regex::new(r"\s*([^\s]+*)\s*").unwrap().captures_iter(INPUT)
		.map(|c| c.get(1).as_ref().map(Match::as_str).unwrap())
		.collect::<Vec<&str>>();

	{
		let mut x = 0;
		for l in lines.iter() {
			if l.chars().cycle().nth(x).unwrap() == '#' {
				p1 += 1;
			}
			x += 3;
		}
	}

	for (dx, dy) in vec![(1,1), (3,1), (5,1), (7,1), (1,2)] {
		let mut x = 0;
		let mut trees = 0;
		for (y,l) in lines.iter().enumerate() {
			if y % dy != 0 {
				continue;
			}
			if l.chars().cycle().nth(x).unwrap() == '#' {
				trees += 1;
			}

			x += dx;
		}
		p2 *= trees;
	}

	println!("{:#?}", p1);
	println!("{:#?}", p2);

	Ok(())
}

const EXAMPLE: &str = "
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
";

const INPUT: &str = "
..#......###....#...##..#.#....
.#.#.....#.##.....###...##...##
..#.#..#...........#.#..#......
..#......#..........###........
...#..###..##.#..#.......##..##
......#.#.##...#...#....###....
..........##.....##..##......#.
......#...........#............
#....#..........#..............
.#........##.............###.##
....#.........#.......#.#....##
#.#..#..#..#.......#...#....##.
.#........#......#.##.......#..
..#.....#####.....#....#..#..##
.......#..##.......#......#.###
..#.#...#......#.##...#........
##...................#...##..#.
......#...#.##...##.#......#..#
.#.................#..##...#...
...#.....#.......##.....#.#....
.......#.#......#.....#..#..##.
..........#........#...........
..#.#..........................
.#.##..#.#...#...#.........#...
.....#....#.....#..#.....#.....
...#.#.#.....#.#..#.......#..#.
.....#...###...##...#......##..
#.###......#.#...#.#.#..###....
#.....#..##......#..........#.#
#...............#........#.#..#
.....#..#.........#......##.#..
.....#.##.##..#..##............
...#......##...............#.#.
.#..#.#............##.#........
#.....#..###.............##.#..
...##..#.#..#...........#..#...
#....#.........#.#.............
##.#.........#..###......#.#..#
...#...#......#.#.#.##..#.##...
.....##............#.##.##..#..
....#................#.##..#..#
...#..#.......#...#..#........#
....#...#...#................#.
....##...............#.#...#...
.#.....###...#.......#.##......
....######.#..............###.#
.#..#.........##...............
................##.#..#....###.
.......#............#.#..#..#..
......#.#...............##.#...
...#..####.#...#..#..#......#..
....#.#...#.....#.........#..##
.##..#...#......##....##.#.#...
.##.#.........##...#....#......
..#.#..#...#.#..#.......#...#.#
.........#..#.....##..#........
..#......#..##.....#..#...###..
..#...#....#.#..#..#.#.#..#.#..
...#..#####.....#......#.......
#.#............#......#..#...#.
.........#..........###.......#
......#....#..#.##.#......#..#.
...........##.#....#.#..#......
..#...................#..#.#...
#....##.............##....#...#
##..#....#.........#..........#
....#.#.#...#..#........#.##..#
...............#...#..##..#....
.##.......#.......#...........#
#.........................##...
#........#.#..#..##..####.#....
...................##.....###..
.#.......#..#......#......#...#
..#.........#...#..........#...
..........#......#....#........
.#......#..#...#..#...##....##.
...#.#..#..#......#.....##.####
.......#.#....#.......#........
#...#.#...##..##.#......#......
.#.........#...................
...#..........#.#......#.......
...#.....##....#..........#....
.#..........##..#..#..##....#.#
.##.#..........#...#.##.......#
#...###....#..#.#...#..#.......
..................##...........
..#...##.#...........#....#.##.
..#......#..##..#....##..#...#.
..#....#.....#.##..#.......#..#
#...#....#..#.#....#......##...
.......##..#..........#........
..#.............##.#.....#...#.
...............#....#...#...##.
##...........#.......#.##......
#..#...........#.........#.....
....###.............###.##..##.
.........#.#.....###.......#...
..#.##....#.#..........#....#..
#........#....##...#..#........
......#..........###..#.#......
.....#.#......##.....#..##...#.
.#.......#......#...#...#...#.#
.#..........##.......#.....##.#
###.#...#....#.....#...#......#
..#.#.#..#.##.#..#.............
.....#.........................
.#..###..#...#...#..#..#...#.#.
#................##...##.##....
......#...#...#..........#...#.
..........#.....##.............
..#.#......#........#.......#..
........##.............#.......
.......#......#.##.#..#........
#.#.#....#........#..........#.
##..##......#..#..#.....#.#..##
##..#..........#...............
#.....##...#.#......#.......#.#
#.....#...#....#..#.....##.....
##..........#.#.....#....#...##
..##.###..#.....#.......#...#..
.#.#.......#......###........#.
.#..............#.#..###.......
.#....#..##.........#..#.#.....
....#....#.#....#..#.......##.#
#.......#.......#.........#....
...#....#....#.....##..#..#.#.#
........#....#...........#.....
.#......##.#.#.##..............
#..#.#.....##........#........#
##...#.#.......##.......#...#..
#...#.....#.##...##.#.....#....
....#..##...#........#.#...#...
...#....#.#.#..###...##.#.....#
......#..#.....#..#........##..
.......#.....#.#.........#.#..#
..#.......#.#.#.#.#....#.##...#
.#...#........#..##..#......#..
.#..#............#...#..#.#....
...##......#......#............
..#...#.#.....#.....#..##.#....
.#......#.#......#..#.#........
..#..........##...#.#.....#..#.
#...#.....#..#...#.............
..##.................#....#....
.#....#.......#..##....#......#
.#....###............##....##.#
##..#........#..#...#.......#..
.....#.....#.#.#.##.........#..
.......#..#....#...#...#.......
...#...#...#.#.#..#.#.....#....
#.#........#..#.##..#..###.....
..................#..#.........
#.#.....#..##.........#.......#
###..#.......#..............#..
......#..#.....###..........#..
....#.#...#..#...........#.#...
...#.....#.......#.....#.#.....
#.....##..#......##...........#
#...###...........##..#...#.##.
......##.##.#...#..#....#......
...#.#......##.#......##....#.#
..............#.#.###.......#..
........#....#.......##..#..###
...#.....##.#....#......##..#.#
..##........#.....#.#..#...#...
.#..#.##.........#.....#...#..#
..#..#....#...........#........
.#...#....................#....
##.....##....#.............#.#.
....#.#..#.#..#.#.#..........##
.............##.#.....#..#..#..
.#....#.....##...#.###.........
..#........#........#.#..###...
.##....#...#...#.......#...#.#.
..#...#...#..##........#..#....
..##.#..#..#.....#......#.#..#.
.#........#..#....#..#.........
..#.#.....#.##..#........###.#.
.....#.##.....##.#.............
#.........#.......#...##...#...
..#.##.#..#..#............#....
.##....#..#............#.....#.
###........##.....##.#...#.....
#......##..##.#.#.#.#.#.#..##..
.....###.....#....#......#....#
........#.........##...#....#.#
.#.#.....#.#..#..##......#...#.
...#.##....#..#.###..#..##.....
....#..........##..#..#..#..#..
...#..#.##..#..#....#.........#
.....#..###.#.....#.....#..#...
......#...#....#.##...#.#......
.#.###..##.....##.##......##...
.....#.#...........#.#.........
#........#...#..#......##.#....
..#.......##....##....#.##.#..#
...###.#.........#......#.....#
..#.##..#....#.....##...#.##...
....##.##.............#...#....
##..#...#..#..#..#.............
.....#.....#.....#.............
...#.##.......#..#.#.....#....#
#.....##.........#......##.....
.....##..........#..#...#..#...
#...###....#.......#...##......
.#....#..#......#.....#...#.#..
#........#.#.#...#.....###.#.##
##...#...##..#..#....#.........
....#............#..#.....#....
#......#.........##....#.......
.#..#..#........#.............#
.##..........#......#.......#..
#............#..#....#.........
....#.#.....#.##...#.....#.#...
...#.#..#...##..#...#.#.#......
#....#..#.........##..#.#.#..##
.#...#..............#.......#..
#...#.....#.#........##......##
...#....##.####.#.........#.#.#
....###.#..#............#.#..#.
....#......#...#......##.#.#.#.
.....#..#.#.##.#...##..........
##..#...#.#...###.............#
....#...#..#.....#.#..#..#..#..
#..........####......#.....###.
.........#........#.##.#...#...
.........#..........#.#..###...
.....##........##.........#...#
..##....#...#.......##.........
.....#.#......##....#...#...#..
.##..#..##.....................
.......#...#..#..#...##....#...
.#...#.......###...#..#..#.....
.......#.....##.##.#.......#..#
.##......#...#....#..#......##.
.##....#..#....#...#...#.......
.........##..#..#.#.#.....##...
...#..............#..#.....####
.#.#.#..#.......#.......#......
..#.#......#..........#........
.#...#.#..#.......#..#..#..#...
.......##.#...#..#....#.....#..
.##...##....##...#........####.
....#.#..##....#...#....#.#....
.....#.....#..#..#.#.##..#.....
..#....#..............#....#...
..#.#.#.....##.#.....#..##.....
....#.....#....#...#...#..#.#..
#...#...........#..#..#........
...#.#..#.........##.#...#..##.
......#.#.........#.#...#......
......#..##.###......##.#....#.
.....#...#..#.......#..........
.#...#.......#.....###......#..
...........##.....#..#..#....#.
..#....#..#...#......#.......#.
..#...#...#.#..#....#...#......
.......#....###.####...###.#...
#.##.#.......#.......#....#.#.#
.##..........#.....#..###......
.....#...........#.##..#....#..
........##.....#.#........##...
#..#..#..................##....
#...###..........#.............
.......#.#.......#.#.......##..
.....#.#...#....#...####.....#.
..##.....##.......#....#.......
##..........#...#..##....##....
..........#..#......#........#.
##..#....#..#....#.....##....#.
##.##.....#...##.##.......#....
..#..#.###.#..##.#..#..#...#...
.#..#.....#........#...##.#....
..#..#.....#.#......##.#.#.....
.#..##...#.#....#...#...#.#.##.
.........#...#....###.#.....#..
...........###.#.#..#..#...#.#.
##...#......##...........#..#..
.........##..#...#.......#.....
#......#.#..........#..#.......
...#.................#....#....
#....#......................##.
##.......#..#......#.#...###.#.
..#....#..#.#......#...........
...#...........###.#.#.........
..#..##.....#.....##...##......
..#..#.#.#.#..#..#..##....#...#
#......##.....##..##.##...#....
#.....#.....#.#........#.......
.#.....#.................#....#
.###....#...#............#.#.#.
.#...#.#......#.#..............
....#...#......#.....#.......#.
........#.....#..........#....#
#..#......#...#...#.........#..
#....#......#...##.#...#...#...
#...#....#....#..#..#.....#..#.
#......##..#..#.#.#..#.#.......
..#..#...............#...##...#
............#..............#.##
.#.#.#......##.......#.......#.
....#.........##.......#...###.
.......#.#...#.#.#.......#.....
....#..#..#...#....#.##.#.##...
...##.##.#...#......#..........
#.....#...#.#...#.##..##.#.....
.......#.....#...#.#...##.#....
.#.............#.....#....##..#
##......#.......#...#....#.....
.###......#.................#..
#.#......##.........##..#......
...#....#..........#.#.........
..##..#.........#..............
.....#...#..................#.#
.............#.........#...#..#
....#....#......#.#.......#...#
#..#............#.#.......#...#
..#.....#............#.........
.#.....................###....#
........#.####.........#.#.#...
#...........##...#.........#..#
...........#..#......#...#.#...
....##...##.....#.....#........
";

