#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;

const INPUT: &[usize] = &[12,20,0,6,1,17,7];
fn main() -> Result<()> {
	println!("p1: {}", p1(INPUT));
	println!("p2: {}", p2(INPUT));
	Ok(())
}

fn p1(input: &[usize]) -> usize {
	let mut spoken = HashMap::new();
	let mut speaking = 0;
	for i in 0..2020 {

		if i < input.len() {
			if i > 0 {
				spoken.insert(speaking, i-1);
			}
			speaking = input[i];
			continue;
		}

		match spoken.get(&speaking).map(|x| *x) {
			None => {
				spoken.insert(speaking, i-1);
				speaking = 0;
			}
			Some(x) => {
				spoken.insert(speaking, i-1);
				speaking = i-1-x;
			}
		}
	}

	speaking
}

fn p2(input: &[usize]) -> usize {
	let mut spoken = HashMap::new();
	let mut speaking = 0;
	for i in 0..30000000 {

		if i < input.len() {
			if i > 0 {
				spoken.insert(speaking, i-1);
			}
			speaking = input[i];
			continue;
		}

		match spoken.get(&speaking).map(|x| *x) {
			None => {
				spoken.insert(speaking, i-1);
				speaking = 0;
			}
			Some(x) => {
				spoken.insert(speaking, i-1);
				speaking = i-1-x;
			}
		}
	}

	speaking
}

#[test]
fn test_p1() {
	assert_eq!(p1(&[0,3,6]), 436);
}
#[test]
fn test_p2() {
	assert_eq!(p2(&[0,3,6]), 175594);
}

