#![feature(iterator_fold_self)]

fn main() {
	let m1 = 1..=5;
	let m2 = (2..=5).flat_map(|x| (1..=5).map(move |y| y * x));
	let v = vec![Box::new(m1) as Box<dyn Iterator<Item = _>>, Box::new(m2)];
	for i in v.into_iter().flat_map(|i| i) {
		println!("{:#?}", i)
	}
}
