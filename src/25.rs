
fn main() {
	println!("p1: {}", p1(9093927, 11001876));
}

fn p1(door: i64, card: i64) -> i64 {
	let mut loop_size = 1;
	let mut prev_pow = 7;
	loop {
		loop_size += 1;
		let next_pow = prev_pow * 7 % 20201227;
		if next_pow == door {
			break transform(card, loop_size);
		}

		prev_pow = next_pow;
	}
}

fn transform(subject: i64, loop_size: i64) -> i64 {
	// subject ^ loop_size mod C
	let mut value = 1;
	for _ in 0..loop_size {
		value = subject * value;
		value = value % 20201227;
	}
	value
}

#[test]
fn test_p1_example() {
	assert_eq!(14897079, p1(5764801, 17807724));
}

