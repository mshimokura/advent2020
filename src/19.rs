#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(iterator_fold_self)]
#![feature(destructuring_assignment)]

use eyre::{Result, Report};
use regex::{Regex, Match};
use itertools::Itertools;
use std::convert::TryFrom;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::RangeInclusive;
use std::iter;
use std::cmp;

fn main() {
	println!("p1: {}", p1(parse(INPUT)));
	println!("p2: {}", p2(parse(INPUT)));
}

fn p1(input: Input) -> usize {
	let map = input.rules.iter()
		.map(|r| (r.id, &r.rhs))
		.collect::<HashMap<_,_>>();

	fn build_re(map: &HashMap<i32,&RuleRhs>, id: i32) -> String {
		match map.get(&id) {
			Some(Leaf(c)) => String::from(*c),
			Some(Children(children)) => {
				"(".to_owned() + &children.iter().map(|c| c.iter().map(|c| build_re(map, *c)).join("")).join("|") + ")"
			},
			_ => panic!("oh no"),
		}
	}

	let re = Regex::new(&("^".to_owned() + &build_re(&map, 0) + "$")).unwrap();

	input.messages.iter()
		.filter(|m| re.is_match(m))
		.count()
}

fn p2(input: Input) -> usize {
	let map = input.rules.iter()
		.map(|r| (r.id, &r.rhs))
		.collect::<HashMap<_,_>>();
	
	// 8: 42 | 42 8
	// 11: 42 31 | 42 11 31

	fn matches<'a>(map: &HashMap<i32,&RuleRhs>, id: i32, message: &'a str) -> Vec<&'a str> {
		match id {
			8 => {
				let mut r = Vec::new();
				let mut prev = matches(map, 42, message);
				loop {
					if prev.len() <= 0 {
						break
					}
					let x = prev.pop().unwrap();
					r.push(x);

					let mut next = matches(map, 42, x);
					prev.append(&mut next);
				}

				r
			},
			11 => {
				let lhs = matches(map, 42, message);
				if lhs.len() == 0 {
					return lhs;
				}

				lhs.iter()
					.flat_map(|rest| {
						iter::once(*rest).chain(matches(map, 11, *rest))
					})
					.flat_map(|rest| matches(map, 31, rest))
					.collect()
			},
			_ => match map.get(&id) {
				Some(Leaf(c)) => if message.chars().nth(0).map(|x| x == *c).unwrap_or(false)
					{ vec![&message[1..]] } else
					{ vec![] },
				Some(Children(or)) => {
					let r = or.iter()
						.flat_map(|and| {
							and.iter()
								.fold(vec![message], |acc, val|
									acc.iter().flat_map(|x| matches(map, *val, x)).collect())
						})
						.collect();
					r
				},
				None => vec![],
			},
		}
	}

	input.messages.iter()
		.filter(|m| matches(&map, 0, m).iter().any(|x| x.len() == 0))
		.count()
}

#[derive(Debug,PartialEq)]
enum RuleRhs {
	Children(Vec<Vec<i32>>),
	Leaf(char),
}
use RuleRhs::*;

#[derive(Debug,PartialEq)]
struct Rule {
	id: i32,
	rhs: RuleRhs,
}

#[derive(Debug,PartialEq)]
struct Input<'a> {
	rules: Vec<Rule>,
	messages: Vec<&'a str>,
}

fn parse(input: &str) -> Input {

	let rule_re = Regex::new(r"(\d+):(.*)").unwrap();
	let leaf_re = Regex::new("\"(.)\"").unwrap();

	let mut result = Input {
		rules: Vec::new(),
		messages: Vec::new(),
	};

	for line in input.split("\n") {
		if line.trim().len() <= 0 {
			continue;
		}
		match rule_re.captures(line) {
			Some(c) => {
				let id = c.get(1).unwrap().as_str().parse().unwrap();
				let rhs_str = c.get(2).unwrap().as_str();
				let rhs = match leaf_re.captures(rhs_str) {
					Some(c) => Leaf(c.get(1).unwrap().as_str().chars().nth(0).unwrap()),
					None => {
						Children(rhs_str.split("|")
							.map(|s| s.trim().split(" ").map(|s| s.parse().unwrap()).collect())
							.collect())
					},
				};
				result.rules.push(Rule {
					id: id,
					rhs: rhs,
				});
			},
			None => {
				result.messages.push(line.trim());
			}
		}
	}

	result
}

#[test]
fn test_parse() {
	assert_eq!(parse("0: 1 | 2\n1: \"a\"\n2: \"b\"\n\nab\nba"), Input {
		rules: vec![
			Rule { id: 0, rhs: Children(vec![vec![1],vec![2]]) },
			Rule { id: 1, rhs: Leaf('a') },
			Rule { id: 2, rhs: Leaf('b') },
		],
		messages: vec!["ab", "ba"],
	});
}

#[test]
fn test_p1_example() {
	assert_eq!(2, p1(parse(r#"
		0: 4 1 5
		1: 2 3 | 3 2
		2: 4 4 | 5 5
		3: 4 5 | 5 4
		4: "a"
		5: "b"

		ababbb
		bababa
		abbbab
		aaabbb
		aaaabbb
	"#)));
}

#[test]
fn test_p2_example_p1() {
	assert_eq!(2, p2(parse(r#"
		0: 4 1 5
		1: 2 3 | 3 2
		2: 4 4 | 5 5
		3: 4 5 | 5 4
		4: "a"
		5: "b"

		ababbb
		bababa
		abbbab
		aaabbb
		aaaabbb
	"#)));
}

#[test]
fn test_p2_8() {
	assert_eq!(2, p2(parse(r#"
		0: 8
		42: "a"

		a
		aaa
		b
	"#)));
}

#[test]
fn test_p2_11() {
	assert_eq!(2, p2(parse(r#"
		0: 11
		42: "a"
		31: "b"
		
		ab
		aaabbb
	"#)));
	/*
	assert_eq!(3, p2(parse(r#"
		0: 11
		42: 1 | 2
		1: "a"
		2: "b"
		31: "b"
		
		ab
		bb
		abbb
	"#)));
	*/
}

#[test]
fn test_p2_11_2() {
	assert_eq!(1, p2(parse(r#"
		0: 11
		42: 1 | 2
		1: "a"
		2: "b"
		31: "b"
		
		abbb
	"#)));
}

#[test]
fn test_p2_example() {
	let input = r#"
		42: 9 14 | 10 1
		9: 14 27 | 1 26
		10: 23 14 | 28 1
		1: "a"
		11: 42 31
		5: 1 14 | 15 1
		19: 14 1 | 14 14
		12: 24 14 | 19 1
		16: 15 1 | 14 14
		31: 14 17 | 1 13
		6: 14 14 | 1 14
		2: 1 24 | 14 4
		0: 8 11
		13: 14 3 | 1 12
		15: 1 | 14
		17: 14 2 | 1 7
		23: 25 1 | 22 14
		28: 16 1
		4: 1 1
		20: 14 14 | 1 15
		3: 5 14 | 16 1
		27: 1 6 | 14 18
		14: "b"
		21: 14 1 | 1 14
		25: 1 1 | 1 14
		22: 14 14
		8: 42
		26: 14 22 | 1 20
		18: 15 15
		7: 14 5 | 1 21
		24: 14 1

		abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
		bbabbbbaabaabba
		babbbbaabbbbbabbbbbbaabaaabaaa
		aaabbbbbbaaaabaababaabababbabaaabbababababaaa
		bbbbbbbaaaabbbbaaabbabaaa
		bbbababbbbaaaaaaaabbababaaababaabab
		ababaaaaaabaaab
		ababaaaaabbbaba
		baabbaaaabbaaaababbaababb
		abbbbabbbbaaaababbbbbbaaaababb
		aaaaabbaabaaaaababaa
		aaaabbaaaabbaaa
		aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
		babaaabbbaaabaababbaabababaaab
		aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
	"#;

	assert_eq!(3, p1(parse(input)));
	assert_eq!(12, p2(parse(input)));
}

const INPUT : &str = r#"
12: "b"
120: 113 12 | 68 106
101: 12 12 | 106 12
104: 12 59 | 106 101
98: 12 78 | 106 125
87: 106 12 | 12 109
102: 12 95 | 106 71
27: 12 84 | 106 127
69: 68 106 | 101 12
75: 3 106 | 79 12
128: 12 74 | 106 17
0: 8 11
118: 12 113 | 106 101
122: 120 106 | 51 12
109: 106 | 12
97: 106 28 | 12 125
70: 106 28 | 12 34
82: 106 40 | 12 38
123: 83 12 | 85 106
129: 61 106 | 126 12
105: 106 37 | 12 47
86: 57 12 | 18 106
35: 12 87 | 106 78
42: 106 2 | 12 88
18: 12 76 | 106 67
45: 12 101 | 106 52
59: 109 12 | 106 106
55: 12 9 | 106 33
32: 12 115 | 106 35
54: 106 28 | 12 23
21: 106 82 | 12 102
33: 12 15 | 106 10
56: 106 1 | 12 98
110: 109 68
100: 80 12 | 58 106
24: 34 12 | 78 106
63: 12 72 | 106 32
13: 113 106 | 59 12
11: 42 31
60: 28 12 | 78 106
46: 107 12 | 89 106
107: 106 81 | 12 62
112: 106 101 | 12 4
124: 106 23 | 12 52
53: 56 12 | 90 106
116: 106 23 | 12 28
125: 106 12
47: 94 106 | 26 12
67: 4 12 | 78 106
30: 28 106 | 34 12
94: 59 106 | 101 12
61: 100 106 | 27 12
10: 106 4 | 12 4
127: 14 106 | 13 12
77: 106 86 | 12 63
71: 106 124 | 12 48
117: 106 111 | 12 45
15: 43 106 | 23 12
58: 65 106 | 54 12
80: 14 106 | 124 12
73: 52 12 | 113 106
88: 77 12 | 46 106
5: 108 106 | 118 12
89: 5 12 | 122 106
99: 28 106 | 125 12
26: 106 125 | 12 52
113: 12 12
41: 50 12 | 6 106
62: 106 110 | 12 103
48: 12 43 | 106 34
50: 124 106 | 49 12
114: 91 12 | 117 106
121: 87 12
39: 106 87 | 12 23
4: 12 106
103: 106 4 | 12 52
106: "a"
119: 106 64 | 12 93
74: 41 12 | 7 106
31: 12 129 | 106 128
34: 106 106
28: 106 106 | 106 12
49: 78 12 | 28 106
93: 34 12 | 34 106
37: 97 12 | 70 106
92: 75 106 | 16 12
6: 10 12 | 20 106
22: 59 109
7: 66 106 | 123 12
95: 73 12 | 22 106
1: 12 52
23: 12 106 | 106 106
25: 69 106 | 39 12
44: 12 34 | 106 23
57: 99 12 | 54 106
65: 52 12 | 78 106
16: 106 104 | 12 22
36: 12 59 | 106 28
38: 20 12 | 60 106
83: 109 101
29: 12 105 | 106 53
91: 12 96 | 106 76
126: 106 114 | 12 19
81: 108 106 | 116 12
78: 12 12 | 106 106
14: 12 101
84: 44 106 | 104 12
64: 12 34 | 106 68
9: 106 83 | 12 67
111: 106 87 | 12 78
115: 87 109
3: 43 106 | 34 12
108: 43 106 | 28 12
51: 87 12 | 125 106
90: 106 36 | 12 121
2: 21 12 | 29 106
68: 12 106 | 106 12
40: 24 106 | 22 12
85: 12 28 | 106 87
52: 106 109 | 12 106
20: 113 106
76: 12 23 | 106 113
17: 12 92 | 106 55
19: 106 119 | 12 25
8: 42
72: 12 69 | 106 30
43: 109 109
66: 39 12 | 112 106
79: 68 12 | 59 106
96: 109 87

bbbaabaaababbaabbbabbbbbaabbabab
bbbbbbbbbaaaabbaabbaabaa
abaaabaaabaaaaaaaabbaaab
abaaaaababaaaaaababbbabb
bababaaaababbaabbbbbabaa
babaaababaababbbbaabbaabbaaabbbabbabbbaa
aabaaaaabaaababaaaabbbbbaababaaabbabbbba
babbbaaababaaabbaababbabaabbaabb
ababbabbbbabababaaaaababbaabaabb
ababbaabbbbbbabaaaabbbaabababbabbbbbbaaaaaabababbbabbbba
aaabbabababbaababbbabaab
bbaababaaaabbbaaabbabbbbabaaaaaaabaaababbbaaababbabababb
aaaababbbaaaabaabbaaabbabbabbbbbbbabbaaaaabbbaaabbbaabbbbbbaaaba
bbaaabbaabbabbabbbaabbababbbbbaabbbabaaa
aaaaaabbabbabbabbbabbbabaaababab
bababaabbbbabaabbaaaabaabaaaaabaababbbaabbbaaabaabbbbabababaaaaababbbbbb
baababaaababaaababbbabaa
ababbbabbababbabbbabbbaa
babaaaaaabaabbabaaaababbaabbabbbaabbaabbabbabbbbabbbabbbbabbbaab
bbabababbaabbababababaaabbbaaababbbbabaa
bbbaabaabaababaabababbabbaaabbbabbbababaaaabaabbaaaaaabbabbbbaba
abbabbaaabbbbbaaaabaababbabaaabbbaabbaaa
ababbaababbbabbbababbaba
abbaaaabbbbaababbbbbabbbababbaaa
bbabbabaababbbabbbaabaaa
abaababbabbbbbaaababbbba
baabbbaabbababbabaabbbab
bbbaabaaaabbbbaabbbabbbb
abababbaaaabbbbbbbbbaabababababa
aaabaabbababbbaaaaaabaabbabaaabaaaababababaabaabaaaaababbbaaaabbaabbaaab
babaabbababbabaabbabbbaa
baaaabbabaabbbaabbbaaaba
babbaababbaaabaabbbaaabbaaaaabababbababa
ababaaaaaaababbabbbabbaa
bbbbbaababaaaaabbabbabaabaabaabababaaabaabaababbbabbabbb
aaaaaaaabaababbbbaaaaaaa
abaabbabaaababaabbaabbbbaaabbbbbbabaaaaa
baababbbabbaababbbbbaabb
abaaabbaababaaabbbbaababababaaaabbbbaaaa
abbabaaabbabbaaababaabab
babaabbaaaaabaabaaabaabbbaaabaababababbbabbaaaba
abbaaaabbaaababababbbbaabbaaaaababbaaaabbaabaaab
bbaaabbbaabbbaabbaaabbba
bbbaabaaaaaababbbaaabaaa
ababbbabbbabbaabbbaabbaa
bbaababbbaaababbaaabbbaabaabbababaaaabbbbaaabbaabbababaabaabaabb
bbababbbabbbabbabbaabbbbabbabbabbbbabbbaabababbb
baaaaabbaabaaaabbbbbaaab
abbbbaabbbabbbabbaaabaab
baabbaabbbaababaababbabbbabbabbb
bbbbababbbbbbaaaabbbbaabaabaabbaaabbbaaa
aabbbabbbbbaabaaabbbbaabaababbaaabbbaaab
aaabbbbbbabbaabbbbaaaabb
baababbbbabbbaababbaaaba
abbbbbaabbabbabababaaabaaabbaabb
bbbaabbbbaaaaabbbbbaabaabaababbbaabbabab
baabbaaabbbaabbabbbabbab
baabababbabbaabaaaaababbabaababa
babbabaababbbaabababaaba
abbbabbaaaaabbbbaabbaababbaaabaabbaabbbaabaabaaa
ababbbabaababbaababbbaabababbbababaabbabaaaabbba
abbbbaaaabbaabbbbaababbbabbbababbbbbbbaabbabaaabbbbaabaa
bbaaabbabbbabababbbaabba
bbbaaabbbabbaabbbbbaaabb
babbabaaabbabbababbbaaab
bbabbabbbbaaaaabbabaabbaaabbbbab
abababbabbbbabbbbabaabbb
aababbaabbaaabbbaaaaaaba
abbbbabbbabbbaaabaaabbbb
aaabaababbaaabaabaabababaababbbbbbabaaab
aaabbabaaaabbbaababaabaaabbbaabbbbbaaaaa
abbbabbabbbbbaababbabbababbbbabbbaaaaaabbbbabbbbbaabbaaaabbbbbbb
bbbbbaabbbababbabaaaaabbaabaaabb
baaaaaabaabbbaaaaabbbbab
bbaaabbababaabaaabaaabababaaaabb
abbaaaaaaaaaabbbaaaaababaaaabbbaabbababb
babaabababbabbbbabbabbabbabaabbbababbbbaabbbbabbbaaababb
aaabaaaabbaabbbbaabaaaba
bbbbbbbbbabaaaaaabababab
abbbbaaaaaababbaababbbababbaabbbabbaabaa
aabbaabaabaaabbabbabbabbaaabbaabbaaaabab
bbabababababbaabbaaabaaa
aabaababaaaaaabbbababbaa
abaabaabbaabbbaaabaabaabbabbbaaabababbababbbbbbbaabbabbb
aabbabaaabbbbaabbaabbabb
abbaaaaabbababbbabbabbbbbbbaababbabbbbabbaaabbabbbbbaaab
abbaababbabbbbaaaabaabaa
bbaaaaabbaababaabaabababbbbbababbaaabbaa
aaabbbaabbabbaababaaaabb
abbabaaabababaaaaaaaaaba
aaabbabaabbabaabbbbbaaab
abbbbabbbbababbaababaabb
bbbabababaaabababbbbbaabbababaabaaaaaaba
bbababbaaaababaaabbbabab
aabbaabaaaabaaaabaabbabaabaaaabb
abaaabbaabbbbbaabbbbbabb
aabbabaababbabaabbaabbabbaababaabbababbaaabbaabbaabbbbab
bbaabbabbabaaaaababbbabb
aababbbaabaabbabbbabbaabababbaaabbbbbbabbaababbabbaabababbabbbabbbbbaaaa
abaaababbaabababaabaabbabbbbaabababbabbaabaabbba
aaabbaabbabbaaabbababaab
babbbbaaabbbbbabbbbabbba
aaaaababaaaaaabbaaabaabb
babaabaababaaabbabaaaaaaabbaaaabbbababaabaabbbab
aabaaaabaaababbabbbaabbb
aaabbabbabbabbbbaaabaaaabbbabaabbaaabaabbabaaabaabaaabbb
abbabbbbabbabbbbabbaaaabbabbabba
aaabbaabaaababbbababbaaabaabababbababbabaaaabaaaabbbabaa
babbaaabbbaabbbbbabbbbab
bababaaaabbaaabbaaabbbabbabababbaaabababbbbabbbaabaababa
aaababaaabbaabbbabbaabbaabbaaaaabbbababaabbaaabb
baabbabbabaababaabbbabbaabaaabaabaabaabbbaabaaba
aaaababbbbbaabababbbaabaabbabaaababaabaaabbbaaaa
ababaaaababbabaaaaabbbbbaaababaaabababab
abaaababbbbbabbbabbbbabbaaaabaabbaaabaab
aaabbabababbaababaabbbba
bbbbbababaaababaabbaaaababbaabbaaabaaaaababbabba
babaaababbbbbaaabbabbaabbbabbabbbbbaababbabbbababbbbbbbabbbaaababababbaa
abbbbbababbbabbbbbbabababaaababbbbabaaaabaabaaaa
babbaabbbabaaababbbababaabbaaaabbbaabbabaabbabaaaabaaabbaabbaaabbbbaaaba
baaaabbaaabaaaabaaaaabaababaabbabbbbbbab
ababbbabbbabbbbbabbaaaaabbbaaabaabbbbbbb
aababbaabaaaabbabbaabbbbaabaaaabaaabbabaaabaabbaaabababb
aababbaaaaabbbaaaabaaaabbbaababbbbaababaababababbaaabbbbabbbaabb
aabaaaaaaabbbbbbaabaaaaa
ababbbbbaaaaaaababaaaaaabbaaabbbabababaabaabaaaaaabbaabb
bbaaabbbbbbbbbbaaaaabbaa
aaabaababaaababbbbbbbaabaababbaaaabbabbbaabababa
bbabbaaaabaaaabaabbababb
baaaabbbbaaabababaaababbaabbaaabaaabbbba
baaaabbbabbaabbbbbbbbaaabababbbb
bbbbbaaabaabbbaababbbbba
baaaabbaababaaabababbaaa
baababbaabbabaabbbbbaaab
baaaabbabbabbaaaaaabbaababaababbababbbba
abbbabbbabaabaabaaaaaabbbabaabaaababaaabbabbaaaabaaabbbb
abbbbbaaabbabaabaabbbaba
bbbbababbabbaaabbbaababbababaaabbaaababa
baabbaabbbabbbabbbabbbababaaaabababbabab
babaaaaababbaaabbbbbabaa
baababaabbabbababababaaaabaaababaaabbaaa
abbaababaabbaabaaababaaa
bbaaabaaabbbbabbabaaaabaabbbbbaabbaaaaba
babaaaaabbaabbababbbabbbaaababbaaabaaabb
babbbaaaabbabbababbaabbabbabaaba
babbbaabbbaababbbaaabbab
aaabbbbbbbbaaabbabababbaababbababababbaa
ababaaaababbbaabbaabbbbbbaaaabab
baababbaabbbbaaabaaabbbb
aaabaabaaaabbbbbabbabaaaaaabbababaabaabaaaabbabaaabaabaaababaaba
bbabbabbaabbabaabbbbaabb
babbaaabaabbbbaaabbbabbaaaababab
aabbabaabbabbbabbbbabaab
aabaaaaaaabaabaaaababbabbbaaaabbaabaababbbbaaabaabababbb
babaabbabbbababaaaaabbbbbbbabbba
bbbaaabbbbabbbbbbaabaaaa
aaabaabaaabbbbbabbbabbab
abbaabababbaaaaabbbaababbabbbbaabaaabababbbbabbaaaabbbbaababbbbabaaaabab
ababaaaabababbbababaaaab
abbbbbababbaabaaaaabbaaabbbabbbaaaabbbabaabababa
bbbbbaabbbbaababbbbbabaa
ababbbaaabbbaabbbaabbbbbbabbbbbbbaaabbabbabaabbaabaabbaa
baaaabaaabbbbabaababbbaa
bbababbaaaaaababbbaabbabbbaaabbabbbaabba
aaaababbabbbbbababaabbaa
bbbaaaabbbbbabbabbbbbababaaabbaabaabbabb
abbbbabaaaababaaaaaabaab
bbaabbabaabbbaababbabaabaabbbbaababbbaabaaabaabaaaababbb
bbbbbaabaababbaabbbbbaabbbbababb
abaaabbabababbbabbbbaaaaabbaabaabaabbaaaaaabbaba
abbaabbababbaabaabaaabbb
aaabbabbabbabaabaaababbb
bbbbababaababbaabbbbaaab
baababaaaaaaaaabbabaaabababbbaabbabbbaba
aaabbababaaababbbbbbababbabaabaabaababaababaaaab
baaaaaababbbabaabaaaaaaaababaabaaabaabaaabbababa
bababbabbbabbabaabbbbaabaabbabab
aaaaabbbaabbbbbaabbbaaab
baaababbaaaaaabbaaaabbbbbbaaabbababbbabb
baaaaabbaaaaaaaaaababaaa
aaabaaaaaabbbabbbababbabaabbbabbaabbabbbbbabaaab
babaaabaaaabbabbaaabbaababababbbbbaabbaa
babbbabaaabbbababaaaaaaa
abbbbaabaababbaaabbbbbaaababbbbbbbbaaaba
aabaaaabaabaababaabbbabbbababaaaaaabbaaa
bbabbbbbabbaaaaabbaaabbabbabaabb
abbabaaababbbaabbbabbbba
abababbaabbbabbbbbabbababaaaabbbbaaabbab
bbabbbababbaaaabbbababbbbaaababbbbabbbaa
bbaabbbbaaabaaaabbababbabbbbbbbbaaaababa
bbabababbbaaabbbababaaabaaaaabbbaaaaaabbabbbbbabaaabaaabbaaabbba
abbbaabababaaaaaaaababaaaaaabbbbbbabbbbbabbbbbbbbaabbaaaaabbbbab
bbbaaaabaaaaabbbbbbbaaab
bababbabbbaaabaaabaababbbaababbabaaaabaabbbbaabb
aaaabbbbbaaaaaabababbbba
aababbaaabbaababababbbabaababbaaabbbaabbaabbabbb
baaaababbbabbaaaaababaab
babbbaaabbaababbabbababa
aabbaaaaababaababbaaaabb
babbabaabbabbabaabbbbaaababbaaaa
abaaaabaabbaaaaaaabbabab
bbaabbbbaaababbaabbabbbbaabbbaabbaaaaaabbabababa
bbaaaaaaabaababbbabababa
baaaabbbaaabbbbbbbabbbba
bbaaaaabaababbaabbbbbbbbbabbaababbbbbbba
baabbbaabbaaaaabaabaababbabaaaaaaaaabbabbbbbabaa
babaabaaabbbbbbbaaaaabbabbbbaaaababaabaabbbbbbabaaaabbaabbbbbbaabaabaaab
babbbaabbbbaabaaaaababaaaaabbaabbbabaaba
bbababbbaaabbbbbbbaaaabb
ababbbbbaaababaaaabbbaaa
ababbaabbbabbbbbaaabaaab
abbabaaaaaabbabbabbaababaabbabba
abbaaaaabbaabbbbaaaaababbbbbbaabbbbabbbaaaabbbab
baaaaabbabbbabbbabbbbaabbabbabaabbaaaaababbaaabbabbbbbbb
bbaaabbbbaaaaababbbaabba
aaabaaaaaabbbbbabaabbbaabaabaababaabaabb
aabaababbaaaabaaaaabaaaababaaabbbabbabab
aaabbaababbbbbababbabbbbbbbbabaa
bbababbbbaababbababbbaaabaabbabb
bbaabbbbbabbaabbbaaabbab
abbbbabbaaabbbaaaaaaabaaabbbabbbabbaababbababbaaaababbbaabaaaabb
abbbbabaaabbabaaaaabaabaabbbbabbbabababababbabba
abbbabbbababaaaaaabababb
baaaaaabbabbabaaabababab
aabbbaababbbbaabaabbabba
aabaaaababaaababbbababaa
ababbbbbaababbabbbaaabab
abbabbaaababbaababbbaaab
aaaabaabaabaabbbbbbabbaa
bbaabbabbaabbbbbababbbba
aaaaabbbaaaaaabbbbaaabbabbbaabaaaabbabbb
abbabbabababbabbbaabbbab
bbaaabbbaaaaabbbaabbabaaaabaabbb
baababbabaaabbbabbbabbba
aabaaabbbbabaaabbaabbabbbbaaababaabababb
bbaaaaaaababbabbbbbabbba
bbaababaabbbbaabbbbabbab
bbabababbbabbaaababbbbaaabaabbabaababbaabbbbaabbbbabaaaa
abbbbbaabbbbbabaaabbabbabaabbbbabbabaaaababaaabbbbaabbabbbbbbabbbbbaabbb
aaababaababbaabaabbaababbaaaaababbabaabaaabababb
bbaaabaaaaaabbbbbbbbaaaaabbbbbaabbbbabbabbabaaabbaaabbaababbbbbb
abbbaababbbaababbabababa
aaaaaaabbaaaabbababbbaaababaaabbabbbabaabbbaabbaababbbba
bbbbaaaaabbbbababababbabbabbabbb
abaababbabbabaaabbbbaaab
baaaaaabbabbabaababbaababababbbb
bbbaaaabababaaaaaabbabab
aaabbaabbaaaaababbaaabbaabaababbaaaaaaabbaaaabbbbbaabbba
baabababaababbabaabbbbab
abbbaababbbaaaabaaabbbbbaaaabbaababababbbbaaaababbbbbabb
aabaabbabaaababbbabbabab
aaababbaaaabbaabaabaabbb
aaababbaaaaaabaaabbaaaaabbbabbab
abbbbbababaabaabbababbbb
aaaabbbbbbababbabbaaabaaaabbbaaa
bbabbaababbaaaaabbabbbbbabbaaabaaaaaaaba
bbababbaaaaaababaaabbbbbbbabbbabbbaabaaa
abababbaaabbbaabbabbbaaabbaabababbaababb
bbabaabaaaabababbbbaabaaabbbabbbbbaabbaaabbbbabaabbbbaaa
baaaaabbbaaaabbabbbbabababaabaabababbabaaaabbbba
bbbbababbbabbaaaaaaabbbbbaababbabaaabbba
ababbabbbabaaaaaabaaaaaabbbbbbba
aaaaabababaaaaaaabbbbabbabaaabaabaaaaaba
aabbbaababbabaababbbabaa
abaabbabaabbaabababaabaabbbbabababbababbabaabbaa
bbbbbbbbbabaabaaaaabaababaababbbababaaaabbbbbbbaabbaaabaaababbbbbabaabbb
abbabbbbabbbabbaaaabaaab
bbbbbbaabaabababbaaaabbbbaaabbaa
ababbbababbbbbaabbaababababbbbba
bababbbabaabbaabbbbbaaab
baabbaabaaaaabaaabababab
abbabbbbaaabbabbaaabbaabbbbbbbbbbaabaabababbabaabbbbaaab
babbaaabbbabbbbbbbbbbbbbaababaab
babbbbaababaaabaababbbba
aaaaabaaabbbbaaabbabbbabaababbbbbaaabaaa
abababbabaabaabaabbabbba
aaaaaabbaababbaaaaaaababbbaabbabaaabbbbaabaaabbbbbbaaaba
ababbbbbabbabbbbabaabbabbabaaabbabaabbabaaababbb
bbaaaaabaabaababaaabaabaabbbbabbbaabbaaa
baaabababbaaabbaaabbbbab
baaaabaabaaaabaaaaaaabababababbabbbbaabaaaabbaabaabaabbbbaabbaaa
aababbabaabbbabbbbbabaab
baabbaabbaaaaabaabaababa
baaababbaaabbabbabbbaabb
baabbbaabbbbaaaaaaabaaab
bbabababbabbbaaaaabaaaba
abbabaababbaabbbaabbaaab
babbaabbaaabbababbababaa
bbabbababbababbbaaabaabb
bababbbaaaabaababaaaabbaabaabbbb
bbbbababbabaaabaabbbbaaabbbaabaaababaaaaabbaaabbbabbbbabaaaababa
ababbaabababbbababbabbbbbbaaabaabbabbbba
aaabaabaaabbaababbbbabbabbbbbbab
babaaaaaababbabbbbbbaaab
bbbbbababbaabbabbaaabaaa
bbababbaabbbbbaabbabbaababbaaaaabbbabaaa
bbabbababbababbbbabbbabb
aaaaababbbbaaabbbababbbaaaabbbaabbbbabbbaaaababaabbbbbbbaaaabaababbbabaa
bbbbaabaababaaabbaaaaaaa
abbabaaaabbbbbaabaabaaab
bbbaaaabbaaababbbaaaaaaa
babbabaabababbabbbaababbabbbaaab
abababbababaaabbaabaabaa
bbabbababbbbbbbbabbaabaa
bbbbaababbbbabbbabbbbbba
aaabbabaabaaabbaababbabbababbbbbaababbba
bbaababbabbbbaabaaabbabbabbaaaaabbaabbbaabbbbbbb
aaaababbbbbbbabaaaababaaabbabbbbabaabaaa
bbababbbababaaabaabbabab
aabaaaabaabbabaabaaabaab
bbaaaaaaaaaababbabaaaabb
aaabbabbbbbbaabaabaabbbb
aaaaabbbabbbbbabaabaaaba
abbbabbbbbbababababbbaaaabbabbbbbabaabbb
abbaaaabbbabbabaababbaaa
abbaababaaabbbbbbabaaabaabbbbbaaabbbaaaaaabababbbababbbbaaaabbabaabaabaa
bbabbababbaabbababbabbabbbbbababaabbbaba
bbabbbababbaaaabaabaababbabababbababbbba
ababbbbbbbabbabbbabbabba
aaabaaababbaababaabaaaaabababaaabaabaabaaababaaaaababbbaabbabaabbbbbabaabaabbbabbababaababaabbaa
aaaaababbabbabaabbbbbabb
aababbaababaaabaaabbabab
bbaaaaabbbababababaababa
aabaaaabaaababaabbaabbba
ababbbbbabaabaabbaabbaabbabbbbab
abbbbabbababbabbabaaaabaaabaabaabaaabbba
abbaaaababbbbaaaabbaabbbaabaaaba
abbbabbabbbbbaabbbabbbbbaaaababa
ababbabbbaaaaabaababaaba
abbaaaaabbbbaabaaabaabaa
bbbaaabbbabbbbaaabbaaaaabaaabbbb
aaaaabababbaabababbababa
aabaababbababbbabbabaabbbbbbbababababaababbaabaaaaaabbbababbabaaaabbabbaaababbabbabbbaaabababaab
aaaaababbbbbabbbbbaabbbbbbaaaababaaaabab
bbaabababbaaaaaababbabbb
bbbaaabbabbbbaaabaaabbba
bbbbbaaabaabbabaabbbbaaaabaabbaabbaaaabb
abbbbbabbaaaabbbbabaaabbaaaaaabbabaaaaababbaaabb
bababbabaababbaaabaababbabaabaaa
bbbbabbbbbaababbbbaaaaabbabbaabbbbababbbaaabaaaaaabbabbb
bababbabababbbabbabbaaabbbaaabbbababaaaabababbabbbabaabb
abaabbabbbbbbaabbaaaabbbabbbbaabbabaaabbbaabbabbbabbbbbbbaaabaaa
baaabababbbaaaabbababbabbabbabaaabbbaaab
bbbaabaabbabbabbaabaaaaa
abaabbabbbababbaaaaabbaa
aabbbbbaabbbabbbaaabaabb
babaaababaabababaabbabba
bbbbabbbbaabbabaaaabbaabbbbaaaabbaaaaaabbaabbaaaaaabbaaabbbbaabbbaaaabab
bbaababaabbbabbbbaaabaab
abaababbbabbaabbbabababa
ababbbababaaaaabbbabaaaa
bbbbbbbbabbbbaababbabbaaaabbbbbabababbbaabbbaaaa
abbabbbbbabbbaaabbbbbbbbaabaabaa
abbabbbbaabbaababaabbababbaababbbaabababaabbbabaaaaabbaa
bbabbbbbbabbbbaabaabbabb
abaaaabaaabbaabaabababaa
aaaaabbbbaaababbbbaabaaa
abbabaaabbaaabbbbababaab
ababbabbbbabababbaabbbba
aaababaabbabbababbbabbaa
bbbbaabaaaabbababbbabbbb
babbaaabbbbbababaaaababa
baababbabaaaabbaabbbbabaaabaabbb
bababbaaabbbbabbbbbaaaaabababaaabbabbaaababbbbbbaababbaabbbbabaaabaabaaabbaaabbaabaababa
bbabbabaaabaabbaaabbbbab
aabbaabaabbabaaaaaaabbaaabbbabaaabbbaaab
abaabbabababaaaababababa
baaaaaaaabaaabbbabbbbaaabbbabaabaaaabaaa
aabaababbbbbbbaaaaaabbab
abbbbbabaaaaaaaabaaabababbbabbbaabbbaaaa
bbbbbaababbbbabbaabbabba
abbabaababaabaabaababaaa
aaababaaabbbaababbabababbbbbaaababaabaaa
aaaaaaaaaabbabaaaababaaa
aaabbabbbabaaabbbbababaa
baaaaabbbbbbbbbbababbbbbaabbaaab
baaaaaabaabbabaabbbbbaabbbababbbbbbbaaab
bbbbbbabaaaaabbbabbaabbbbbabbbbbbabbbbbababbbaabbbbabbaa
babbaabaaaaabaabbabaabbbbbbaaaaaaaababab
bbaababbabbaababbbabbabaababbaaabbbabbaa
aaababaaababbaabaaaaabba
bbbbbbbbaaaababbbbaabaaa
bbababbabbbbbbaaaabaabbb
bbbaaabbaaabbaabbaaaaaaa
aaababbaaaabbababbabbbbbbaabbaabbbbbaaab
babbaababaababbbbbbbaaaabbaaabaabbaabbba
bbbaababbaaaabbbbababaab
bbaababbbbaabbbbbbaabbaa
babaaaaaabbabaaaabbabaabbbbbbabbaabbbbab
baabbbbbabbbbabaaaababbb
baabbabaabbaabbbbabbabbb
abbaabbbaaababbaabbbbbabbbaabbababababbabbbbbbabbabaababaabbbbbb
aaaabbbbaabbbaabbaaaaabbabbbbaaababaaaaaabbbaabb
baababbbbbaababaabbabbaabbbbababbabbaabaaaaababbbaabbbbaabababaa
baabababaabbbbaabbabbbaa
aaaaaaabbaaababbaaaabaab
aabaababaaabaaaabbbbbbaaaaaabaaa
baabbaabaaaababbbbaaaabb
abbabbbbaaabbaababbbabab
aaabbababbabababaabbabab
babaabaaaaaaabaabbaababbaabbbbbaaabbbbaaabbababbbabaabbbbbabaabbbbababaa
bbbbabbbaabbbbbabaabaabb
aaabbbaaaaabbabababbbaabbababaaaabaabbaa
baaaaababababbbabaabaababaaabbab
aaaaaaababbbabbababaaabbaaaababaabbbbbba
abbbaabaabaaabbaaabbaabb
aabbaabababaabaabbaabbaa
bbababbbabababbabbaabbaa
bbbbbababbabbaababbbbabaaaaaabbbaabababaaaabaaab
baaababbababbaabbbaabaab
baabaababaabbabaaabbaaaaabaaaabb
abbabbbbababbaabbbababaa
bbbbaabababaaabbaababbaababbaaabbbaaabab
aabaababbaababaabaaaabbbabbaaabb
babbbaaababbaaabbababbbababbbbab
bbabbbababaaabaaababbabbbbaaaaaababbbbbb
aaaaaaababbabbbbabaaababbaabababbbbaaaba
abababbabaababaabbababaa
baaaabbbbbabbaabbabaaabababaaabbabaababbabaabbba
bbbbabbbbbaabbbbbbaaaabb
bbababbbabaabaababbaaabb
bbaaabaababaaabababbabab
baaaaabbaaaababbaababbbb
aaaabbbbabbbbaaababbbbbb
abbbbbaaabbabbaabbbbbaaaababbbbaaababbba
abbbbaaababaabaaabaabbbb
abaabaabbaabbaabaaabbabaaabbabba
ababaaababbbbabbaaababaabaabbbaaaabbaaaa
babbbbaaabaaababababbbabaaaaabba
bbabbbababbaabbbaaaabbbbabbaaaaaabaabbaababbbbbb
aabbaabbbaababbaabbabbbbabaabaaaabbaaababaaaabbb
bbbbbbaaabaaaaaaaaaaabba
abaaabaaabbaabbbababaaaaaabbbbab
bbaaabbbbaabababbbbababababaabaaabaaaababbaaabba
baababaabbbaaabbaaabbabbaaaaaaaaaaabaaaaaabbabab
bbaabbbbbabbabaabaabaabb
bbbaababaaaaaaaababbbaabaababbbbbaaaabab
baabbabaabbbabbabaabaabaababbbabbaabbbaabbababbbbabbbababbbbbabbbbabaaaa
baabbbbbabbaababbbbaaaabaababbaabbbbabbbabaaabaaaaabababbabbbbabababaabbbaabaaab
abaaabababbbbababaabbbaaaababaaaaabaabaa
bababababbbabbaaabbaaabb
baaaaabbabbbbaabaabaaaba
aaaaababbbaabbbbbababaaaaaababbabbaaaabbbbbabbaa
bababaaaaaaaababababbbba
bbaaabbaaaaaaaaaaababaaa
babbaabbabbbabbbbbbbabbbabbabbbb
babbbaaaaaaaaaaabbaabaab
abbabbaabbabbababbbbaaaaabbaaaba
abbabbababbaaaaabbaaabbaabbaabbabbbbaaab
aaababbababaaabbbaaaabab
aaaaaaabababbabbabbaabbbaabbbbbb
abbbaabaababaaaaaaaabbaa
abaaabbabbbbaaaabbbababb
bbbbbaababbbbabbbabaaaaabbbbbbaaabbabaababaabaaabbaaaababaabbbba
baabbbbbabbbbaaabbabbbabbaabababbbbbbabbbabbabba
aaaaabbbbabbabaaaaababbb
babbbbaabbbbbaabbbabbbbbabbbbbaabbbbbbaaaabbaaaa
abbabaaaaaababaabbabbbabaabbbbbaabbbabbbbabbbbba
abbaabbbbbabbaaaaabbbabbaabbabaaaababbbaaababaab
"#;
