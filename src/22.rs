#![allow(dead_code)]

use std::collections::{HashSet};
use itertools::Itertools;

fn main() {
	println!("p1: {}", p1(parse(INPUT)));
	println!("p2: {}", p2(parse(INPUT)));
}

fn p1(mut decks: Vec<Vec<i64>>) -> i64 {
	loop {
		if decks.iter().any(|x| x.len() == 0) {
			break decks.iter()
				.map(|d| {
					let count = d.len();
					d.iter().enumerate()
						.map(|(i, card)| ((count-i) as i64) * card)
						.sum()
				})
				.filter(|score| *score > 0)
				.nth(0).unwrap()
		}
		
		let top = decks.iter_mut().map(|deck| deck.remove(0)).collect::<Vec<_>>();
		let i = top.iter().position_max().unwrap();
		let winner = top[i];
		decks[i].push(winner);
		for card in top {
			if card < winner {
				decks[i].push(card);
			}
		}
	}
}

fn p2(mut decks: Vec<Vec<i64>>) -> i64 {
	fn play(decks: &mut Vec<Vec<i64>>) -> usize {
		let mut prev_decks = HashSet::new();
		
		loop {
			prev_decks.insert(decks.clone());

			if decks.iter().any(|x| x.len() == 0) {
				break decks.iter().enumerate()
					.filter(|(_,deck)| deck.len() > 0)
					.map(|(i,_)| i)
					.nth(0).unwrap()
			}

			let top = decks.iter_mut().map(|deck| deck.remove(0)).collect::<Vec<_>>();
			
			let winner = if top.iter().zip(decks.iter()).all(|(top,deck)| deck.len() as i64 >= *top) {
				let mut next = decks.clone();
				next.iter_mut().zip(top.iter())
					.for_each(|(next, top)| next.truncate(*top as usize));
				play(&mut next)
			} else {
				top.iter().position_max().unwrap()
			};

			let winning_card = top[winner];
			decks[winner].push(winning_card);
			for card in top {
				if card != winning_card {
					decks[winner].push(card);
				}
			}

			if prev_decks.contains(decks) {
				break 0
			}
		}
	}

	let winner = play(&mut decks);
	let count = decks[winner].len();
	decks[winner].iter().enumerate()
		.map(|(i, card)| ((count-i) as i64) * card)
		.sum()
}

fn parse(input: &str) -> Vec<Vec<i64>> {
	let mut decks = Vec::new();
	let mut deck = Vec::new();
	for line in input.split("\n") {
		if line.len() == 0 {
			continue;
		}

		if line.starts_with("Player") {
			if deck.len() > 0 {
				decks.push(deck);
				deck = Vec::new();
			}
		} else {
			deck.push(line.parse().unwrap());
		}
	}

	if deck.len() > 0 {
		decks.push(deck);
	}

	decks
}

#[test]
fn test_p1_example() {
	let input = parse(EXAMPLE);
	println!("{:?}", &input);
	assert_eq!(306, p1(input));
}

#[test]
fn test_p2_example() {
	assert_eq!(291, p2(parse(EXAMPLE)));
}

const EXAMPLE: &str = "
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
";

const INPUT: &str = "
Player 1:
12
40
50
4
24
15
22
43
18
21
2
42
27
36
6
31
35
20
32
1
41
14
9
44
8

Player 2:
30
10
47
29
13
11
49
7
25
37
33
48
16
5
45
19
17
26
46
23
34
39
28
3
38
";
